<?php
/*
Template Name: Basket Page
*/
get_header();

if(isset($_SESSION['basket'])) {
  $baskets = $_SESSION['basket'];
} else {
  $baskets = '';
}
//dd($baskets);
$basketImages = [];
$basketVideos = [];
$basketMarCollaterals = [];
$basketLogos = [];
$basketallimages = [];
$basketallvideos = [];
$basketallmarketing = [];
$basketalllogos = [];
$basketallassets = [];

foreach ($baskets as $key=>$resorts) {
  if (array_key_exists("resort_images",$resorts)){
    array_push($basketImages, $baskets[$key]);
  }
  if (array_key_exists("video",$resorts)){
    array_push($basketVideos, $baskets[$key]);
  }
  if (array_key_exists("marketing",$resorts)){
    array_push($basketMarCollaterals, $baskets[$key]);
  }
  if (array_key_exists("logo",$resorts)){
    array_push($basketLogos, $baskets[$key]);
  }
}


$theme_path = get_template_directory_uri();
$CurrentUrl = get_home_url();
?>
<div class="main_container_basket edit-s">
  <div class="content_container_left col-lg-9 col-md-9 col-sm-9 col-xs-12">
    <div class="header_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="header_row row">
        <div class="main_header">
          <div class="sub_header_icon">
            <h1>YOUR BASKET</h1><img src="" width="40" style="margin-top: -15px;">
          </div>
        </div>
      </div>
    </div>


    <!--***************************************************-->
    <!--  RIGHT COLUMN STARTS -->


    <!-- <div class="left_col col-lg-3 col-md-12 col-sm-12 col-xs-12">
    <div class="content_row_search row">
    <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="content_row_search_inner row">
    <div class="left_col_detail">
    <div class="left_col_detail_header">
    <h4>DOWNLOAD OPTIONS</h4>
    <h5>Versions available for download:</h5>

    <div class="left_col_detail_list">
    <ul class="full-ul">
    <li><div class="selected">IMAGE ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
    <li><div class="non_selected">VIDEO ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
    <li><div class="non_selected">MARKETING COLLATERAL ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
    <li><div class="non_selected">LOGOS & MOTIFS ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
    <li><div class="non_selected">ALL ASSETS <input type="radio" class="radiobtn" name="basket_radio"></div></li>
  </ul>
</div>



</div> -->



<!--<div class="left_col_detail_list_2">
<span>Your basket is currently: 10MB</span><br>
<span>The maximum basket size is: 2GB</span>
</div>-->


<!-- </div>

<div class="menu_detail_icons">
<a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_delete.png" class="image_detail_icons"></a> -->
<!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
<!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_download.png" class="image_detail_icons"></a>
</div>


</div>
</div>
</div>
</div> -->


<!--  RIGHT COLUMN ENDS -->
<!--***************************************************-->
<!--  LEFT COLUMN STARTS -->


<div class="content_container_right container col-lg-9 col-md-12 col-sm-12 col-xs-12">
  <div class="row col_row">
    <div class="sub_header_2">
      <div class="sub_header_content">
        IMAGES
      </div>

      <div class="sub_header_icon">
        <img src="<?php echo $theme_path; ?>/assets/img/camera_icon.png" width="25" style="margin-top: -10px;">
      </div>

    </div>
    <div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <?php foreach ($basketImages as $basketItem): ?>
        <?php $tempdata = []; ?>
        <?php $imagedetail =  getFileByKeyword( $basketItem['keyword'], $basketItem['postid'], 'image' ,'add_low_resolution_web');?>

        <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
          <div class="">
            <a href="<?php echo get_permalink($basketItem['postid']).'?type=image&slug='.$basketItem['keyword']; ?>"><img src="<?php echo $imagedetail['url'] ?>" alt="Avatar" class="image" style="width:100%"></a>
            <div class="image_detail_search">
              <h4><b><?php echo get_the_title($basketItem['postid']); ?></b></h4>
              <h5><?php echo $imagedetail['name'] ?></h5>
              <div class="ref_detail">
                <div class="ref_detail_text">
                  <span>Downloaded: 26-05-2017</span><br>
                  <span>Maximum Resolution: 5000 x 3000 TIFF</span>
                </div>
                <div class="detail_icons">
                  <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                  <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                  <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="image-download">
                    <input type="hidden" name="action" value="single_resort_download_assets">
                    <input type="hidden" name="postid" value="<?php echo $basketItem['postid'] ?>">
                    <input type="hidden" name="keyword" value="<?php echo $basketItem['keyword'] ?>">
                    <input type="hidden" name="type" value="resort_images">
                    <?php
                    $tempdata['postid'] = $basketItem['postid'];
                    $tempdata['keyword'] = $basketItem['keyword'];
                    $tempdata['type'] = 'resort_images';
                    $tempdata['files'] = [];
                    foreach ($basketItem['resort_images'] as $image) {
                      echo '<input type="hidden" name="images[]" value="'.$image.'">';
                      array_push($tempdata['files'],$image);
                    }
                    array_push($basketallimages,$tempdata);
                    array_push($basketallassets,$tempdata);
                    $tempdata = [];
                    ?>
                    <?php if(is_user_logged_in()) : ?>
                      <button type="submit" class="download-btn cart-detail">
                        <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                        <!-- <span class="image_basket_count pl-1"><?php echo count($basketItem['resort_images']) ?></span> -->
                      </button>
                    <?php else: ?>
                      <a class="download-btn" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                        <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          <!-- <span class="image_basket_count cart-detail-count pl-1"><?php echo count($basketItem['resort_images']) ?></span> -->
                      </a>
                    <?php endif; ?>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      <?php endforeach; ?>
      <?php //dd($basketallimages); ?>






      <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="header_row row">
          <div class="sub_header">
            <div class="sub_header_content_2">
              VIDEOS
            </div>

            <div class="sub_header_icon">
              <img src="<?php echo $theme_path; ?>/assets/img/video_icon.png" width="25" style="margin-top: -10px;">
            </div>
          </div>
        </div>
      </div>

      <?php foreach ($basketVideos as $basketItem): ?>
        <?php $tempdata = []; ?>
        <?php $videodetail =  getFileByKeyword( $basketItem['keyword'], $basketItem['postid'], 'video' ,'add_low_resolution_video_360p');?>
        <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
          <div class="">
            <a href="<?php echo get_permalink($basketItem['postid']).'?type=video&slug='.$basketItem['keyword']; ?>">
              <video width="100%" height="auto" controls controlsList="nodownload">
                <source src="<?php echo $videodetail['url'] ?>" type="video/mp4">
                  Your browser does not support HTML5 video.
                </video>
              </a>
              <div class="image_detail_search">
                <h4><b><?php echo get_the_title($basketItem['postid']); ?></b></h4>
                <h5><?php echo $videodetail['name'] ?></h5>
                <div class="ref_detail">
                  <div class="ref_detail_text">
                    <span>Downloaded: 26-05-2017</span><br>
                    <span>MP4 300KB</span>
                  </div>
                  <div class="detail_icons">

                    <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                    <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                    <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="video-download">
                      <input type="hidden" name="action" value="single_resort_download_assets">
                      <input type="hidden" name="postid" value="<?php echo $basketItem['postid'] ?>">
                      <input type="hidden" name="keyword" value="<?php echo $basketItem['keyword'] ?>">
                      <input type="hidden" name="type" value="video">
                      <?php
                      $tempdata['postid'] = $basketItem['postid'];
                      $tempdata['keyword'] = $basketItem['keyword'];
                      $tempdata['type'] = 'video';
                      $tempdata['files'] = [];
                      foreach ($basketItem['video'] as $video) {
                        echo '<input type="hidden" name="videos[]" value="'.$video.'">';
                        array_push($tempdata['files'],$video);
                      }
                      array_push($basketallvideos,$tempdata);
                      array_push($basketallassets,$tempdata);
                      $tempdata = [];
                      ?>
                      <?php if(is_user_logged_in()) : ?>
                        <button type="submit" class="download-btn cart-detail">
                          <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          <!-- <span class="image_basket_count pl-1"><?php echo count($basketItem['video']) ?></span> -->
                        </button>
                      <?php else: ?>
                        <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                          <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          <!-- <span class="image_basket_count cart-detail-count pl-1"><?php echo count($basketItem['video']) ?></span> -->
                        </a>
                      <?php endif; ?>
                    </form>



                    <!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a> -->
                    <!-- <a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                    <!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a> -->
                    <!-- <span class="image_basket_count">(<?php echo count($basketItem['video']) ?>)</span> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

        <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="header_row row">
            <div class="sub_header">
              <div class="sub_header_content_2">
                MARKETING COLLATERAL
              </div>

              <div class="sub_header_icon">
                <img src="<?php echo $theme_path; ?>/assets/img/marketing_icon.png" width="25" style="margin-top: -10px;">
              </div>
            </div>
          </div>
        </div>

        <?php foreach ($basketMarCollaterals as $basketItem): ?>
          <?php $tempdata = []; ?>
          <?php $marCollateral =  getFileByKeyword( $basketItem['keyword'], $basketItem['postid'], 'marketing', 'add_marketing_collateral_file' );?>
          <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="">
              <a href="<?php echo get_permalink($basketItem['postid']).'?type=marketing&slug='.$basketItem['keyword'] ?>;"><img src="<?php echo $marCollateral['url']; ?>" alt="Avatar" class="image" style="width:100%"></a>
              <div class="image_detail_search">
                <h4><b><?php echo get_the_title($basketItem['postid']); ?></b></h4>
                <h5><?php echo $marCollateral['name'] ?></h5>
                <div class="ref_detail">
                  <div class="ref_detail_text">
                    <span>Downloaded: 26-05-2017</span><br>
                    <span>PDF 900KB</span>
                  </div>
                  <div class="detail_icons">

                    <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                    <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                    <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="marketing-download">
                      <input type="hidden" name="action" value="single_resort_download_assets">
                      <input type="hidden" name="postid" value="<?php echo $basketItem['postid'] ?>">
                      <input type="hidden" name="keyword" value="<?php echo $basketItem['keyword'] ?>">
                      <input type="hidden" name="type" value="marketing">
                      <?php
                      $tempdata['postid'] = $basketItem['postid'];
                      $tempdata['keyword'] = $basketItem['keyword'];
                      $tempdata['type'] = 'marketing';
                      $tempdata['files'] = [];
                      foreach ($basketItem['marketing'] as $marketing) {
                        echo '<input type="hidden" name="marketing[]" value="'.$marketing.'">';
                        array_push($tempdata['files'],$marketing);
                      }
                      array_push($basketallmarketing,$tempdata);
                      array_push($basketallassets,$tempdata);
                      $tempdata = [];
                      ?>
                      <?php if(is_user_logged_in()) : ?>
                        <button type="submit" class="download-btn cart-detail">
                          <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons ">
                          <!-- <span class="image_basket_count pl-1"><?php echo count($basketItem['marketing']) ?></span> -->
                        </button>
                      <?php else: ?>
                        <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                          <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          <!-- <span class="image_basket_count cart-detail-count pl-1"><?php echo count($basketItem['marketing']) ?></span> -->
                        </a>
                      <?php endif; ?>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

        <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="header_row row">
            <div class="sub_header">
              <div class="sub_header_content_2">
                LOGOS & MOTIFS
              </div>

              <div class="sub_header_icon">
                <img src="<?php echo $theme_path; ?>/assets/img/camera_icon.png" width="25" style="margin-top: -10px;">
              </div>
            </div>
          </div>
        </div>

        <?php foreach ($basketLogos as $basketItem): ?>
          <?php $tempdata = []; ?>
          <?php $logodetail =  getFileByKeyword( $basketItem['keyword'], $basketItem['postid'], 'logo','add_low_resolution_logo_print_72kb' );?>
          <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="">
              <a href="<?php echo get_permalink($basketItem['postid']).'?type=logo&slug='.$basketItem['keyword']; ?>"><img src="<?php echo $logodetail['url'] ?>" alt="Avatar" class="image" style="width:100%"></a>
              <div class="image_detail_search">
                <h4><b><?php echo get_the_title($basketItem['postid']); ?></b></h4>
                <h5><?php echo $logodetail['name'] ?></h5>
                <div class="ref_detail">
                  <div class="ref_detail_text">
                    <span>Downloaded: 26-05-2017</span><br>
                    <span>Maximum Resolution: 5000 x 3000 TIFF</span>
                  </div>
                  <div class="detail_icons">

                    <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                    <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                    <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="image-download">
                      <input type="hidden" name="action" value="single_resort_download_assets">
                      <input type="hidden" name="postid" value="<?php echo $basketItem['postid'] ?>">
                      <input type="hidden" name="keyword" value="<?php echo $basketItem['keyword'] ?>">
                      <input type="hidden" name="type" value="logo">
                      <?php
                      $tempdata['postid'] = $basketItem['postid'];
                      $tempdata['keyword'] = $basketItem['keyword'];
                      $tempdata['type'] = 'logo';
                      $tempdata['files'] = [];
                      foreach ($basketItem['logo'] as $logo) {
                        echo '<input type="hidden" name="logos[]" value="'.$logo.'">';
                        array_push($tempdata['files'],$logo);
                      }
                      array_push($basketalllogos,$tempdata);
                      array_push($basketallassets,$tempdata);
                      $tempdata = [];
                      ?>
                      <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
                      <?php if(is_user_logged_in()) : ?>
                        <button type="submit" class="download-btn cart-detail">
                          <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          <!-- <span class="image_basket_count pl-1"><?php echo count($basketItem['logo']) ?></span> -->
                        </button>
                      <?php else: ?>
                        <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                          <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          <!-- <span class="image_basket_count cart-detail-count pl-1"><?php echo count($basketItem['logo']) ?></span> -->
                        </a>
                      <?php endif; ?>
                    </form>

                    <!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                    <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
                    <span class="image_basket_count">(<?php echo count($basketItem['logo']) ?>)</span> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <div class="left_col col-lg-3 col-md-12 col-sm-12 col-xs-12">
    <div class="content_row_search row">
      <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="content_row_search_inner row">
          <div class="left_col_detail">
            <div class="left_col_detail_header">
              <h4>DOWNLOAD OPTIONS</h4>
              <h5>Versions available for download:</h5>
              <div class="left_col_detail_list">
                <ul class="full-ul">
                  <ul class="full-ul">
                    <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="all-download" method="POST">
                      <input type="hidden" name="action" value="basket_bulk_download_assets">
                      <li>
                        <a href="#" class="selected">Images Only <input type="checkbox" class="chk" name="alldownload">
                        </a>
                        <?php //dd(json_encode($allimages)) ?>
                        <input disabled type="hidden" name="basketallimages" value='<?php echo json_encode($basketallimages); ?>'>
                      </li>
                      <li>
                        <a href="#" class="non_selected">Video Only <input type="checkbox" class="chk" name="alldownload">
                        </a>
                        <input disabled type="hidden" name="basketallvideos" value='<?php echo json_encode($basketallvideos); ?>'>
                      </li>
                      <li>
                        <a href="#" class="non_selected">Marketing Collateral Only <input type="checkbox" class="chk" name="alldownload">
                        </a>
                        <input disabled type="hidden" name="basketallmarketing" value='<?php echo json_encode($basketallmarketing); ?>'>
                      </li>
                      <li>
                        <a href="#" class="non_selected">Logos & Motifs Only <input type="checkbox" class="chk" name="alldownload">
                        </a>
                        <input disabled type="hidden" name="basketalllogos" value='<?php echo json_encode($basketalllogos); ?>'>
                      </li>
                      <li>
                        <a href="#" class="non_selected">All Assets <input type="checkbox" class="chk" name="alldownload">
                        </a>
                        <input disabled type="hidden" name="basketallassets" value='<?php echo json_encode($basketallassets); ?>'>
                      </li>
                    </ul>
                  </ul>
                </div>
              </div>
            </div>
            <div class="menu_detail_icons">
              <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_delete.png" class="image_detail_icons"></a>
              <?php if(is_user_logged_in()) : ?>
                <button type="submit" class="download-btn">
                  <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                </button>
              <?php else: ?>
                <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                  <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                </a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <!-- <div class="left_col col-lg-3 col-md-12 col-sm-12 col-xs-12">
  <div class="content_row_search row">
  <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="content_row_search_inner row">
  <div class="left_col_detail">
  <div class="left_col_detail_header">
  <h4>DOWNLOAD OPTIONS</h4>
  <h5>Versions available for download:</h5>

  <div class="left_col_detail_list">
  <ul class="full-ul">
  <li><div class="selected">IMAGE ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
  <li><div class="non_selected">VIDEO ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
  <li><div class="non_selected">MARKETING COLLATERAL ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
  <li><div class="non_selected">LOGOS & MOTIFS ONLY <input type="radio" class="radiobtn" name="basket_radio"></div></li>
  <li><div class="non_selected">ALL ASSETS <input type="radio" class="radiobtn" name="basket_radio"></div></li>
</ul>
</div>



</div> -->



<!--<div class="left_col_detail_list_2">
<span>Your basket is currently: 10MB</span><br>
<span>The maximum basket size is: 2GB</span>
</div>-->


<!-- </div>

<div class="menu_detail_icons">
<a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_delete.png" class="image_detail_icons"></a> -->
<!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
<!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_download.png" class="image_detail_icons"></a>
</div>


</div>
</div>
</div>
</div> -->
</div>


</div>








<?php
get_footer();
?>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function(){
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  }
}

var acc = document.getElementsByClassName("accordion_2");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function(){
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  }
}
</script>

<script>
// When the user clicks on div, open the popup
function myFunction() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}
$(document).ready(function() {
  $('.chk').click(function() {
    $('.chk').each(function() {
      console.log($(this).parent().parent());
      if($(this).prop("checked")){
        $(this).parent().parent().children('input').prop('disabled', false);
      }else{
        $(this).parent().parent().children('input').prop('disabled', true);
      }
    });
  });
  $("#all-download").validate();
});
</script>
