<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oneandonly_newdev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ukomopqxfa3bwxnpyvgnkwfwfqp4wlfkcyydmtkohch8ypoaydssromlujtseflx');
define('SECURE_AUTH_KEY',  'ir1sc54o2jyavb8gj8vpbes9xf1911qhn5xy4akdinxumggbsunpyb3o9pe5eoyu');
define('LOGGED_IN_KEY',    'uvar81rhvsvdchf6seuvdbht9hzkrbt08qtrbguceckukjlwgq4k6gh1xwofbppd');
define('NONCE_KEY',        'booz5zweuzp3fr6a4i7vahsg8elxokhoqpodd6xr2v77kfsxbajdkhcevmeu3uhk');
define('AUTH_SALT',        'volkdvmle4iwrfat66qiqgxgrcslc78cib5do1owida92vzdakyrbxdorbbbc5ot');
define('SECURE_AUTH_SALT', 's46v1ijd54wm4ahkg6kqygll2fwt4fqtug41kjkx8po5lfoobukubbduyxdcgnbs');
define('LOGGED_IN_SALT',   'sb5d7g1ttgcdoegerzdoz3ryun07x7tlguzgctuugxrdoqpq6nw7gjq62anhozbp');
define('NONCE_SALT',       'rd4l7vvusxn0bazqzs4h15ff1tmvbakteaj7kgjd5bc2zeloxrk5shtcl8hzt7hs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';


define('FS_METHOD','direct');
define ('ALLOW_UNFILTERED_UPLOADS',true);
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

// if (!session_id())
// 	    session_start();

// if(!isset($_SESSION['basket'])){
// 	$_SESSION['basket'] = [];
// }
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
