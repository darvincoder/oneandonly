jQuery(function ($) {

    var selected_category = '';


    $('#image_data_reapeter .resort_image_main_category select').each(function () {
        console.log('hi');
    	var $current = $( this );
        var term_id = $current.val();
        var data_id = $current.closest('.acf-row').attr("data-id");
        var post_id = $("#post_ID").val();
        $current.closest('.acf-field').next('.acf-field').find('select').html('<option value="none">Please wait</option>');
        if(term_id != 0){
        	data = {
	            action: 'subCategory_by_mainCategory',
	            pa_nonce: pa_vars.pa_nonce,
	            main_category: term_id,
	            field_data_id: data_id,
	            post: post_id
	        };
	        jQuery.post(ajaxurl, data, function (response) {
            console.log(response);
            var subcat = JSON.parse(response);
	        	$current.closest('.acf-field').next('.acf-field').find('select').html(subcat.html);

            if( subcat.catid != "none"){
              data2 = {
    	            action: 'supSubCategory_by_subCategory',
    	            pa_nonce: pa_vars.pa_nonce,
    	            main_category: subcat.catid,
    	            field_data_id: data_id,
    	            post: post_id
    	        };
              jQuery.post(ajaxurl, data2, function (response) {
                console.log("darvin");
                $current.closest('.acf-field').next('.acf-field').next('.acf-field').find('select').html(response);
              });
            }
	        });
        }
    });

    $('#image_data_reapeter').on("change click", '.resort_image_sub_category select', function () {
      var $current = $( this );
        var term_id = $current.val();
        $current.closest('.acf-field').next('.acf-field').find('select').html('<option value="none">Please wait</option>');
        data = {
            action: 'supSubCategory_by_subCategory',
            pa_nonce: pa_vars.pa_nonce,
            main_category: term_id,
        };
        jQuery.post(ajaxurl, data, function (response) {
        	$current.closest('.acf-field').next('.acf-field').find('select').html(response);
        });

    });


    $('#image_data_reapeter').on("change", '.resort_image_main_category select', function () {
    	var $current = $( this );
        var term_id = $current.val();
        $current.closest('.acf-field').next('.acf-field').find('select').html('<option value="none">Please wait</option>');
        data = {
            action: 'subCategory_by_mainCategory',
            pa_nonce: pa_vars.pa_nonce,
            main_category: term_id,
        };
        jQuery.post(ajaxurl, data, function (response) {
          var subcat = JSON.parse(response);
        	$current.closest('.acf-field').next('.acf-field').find('select').html(subcat.html);
        });

    });
});
