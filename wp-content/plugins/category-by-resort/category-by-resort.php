<?php

/*
 *
 *  Plugin Name: Category By Resort
 *  Description: This plugin is used for fetching sub resort category from parent category.
 *  Author: Sagar Prajapati (sagarp4412@gmail.com)
 *
 */

function my_acf_admin_head($hook) {

    $dir = plugin_dir_url(__FILE__);
    wp_enqueue_script('populate-category', $dir . 'js/autopopulatecategory.js');

    wp_localize_script('populate-category', 'pa_vars', array(
        'pa_nonce' => wp_create_nonce('pa_nonce'), // Create nonce which we later will use to verify AJAX request
            )
    );
}

add_action('wp_ajax_subCategory_by_mainCategory', 'subCategory_by_mainCategory');
add_action('wp_ajax_nopriv_subCategory_by_mainCategory', 'subCategory_by_mainCategory');

function subCategory_by_mainCategory($selected_category) {

    global $post;
    $result = [];
    $postid = $post->ID;
    if($postid == ''){
        $postid = $_REQUEST['post'];
    }
    $main_category_id = $_POST['main_category'];

    $terms_sub = get_terms( array(
        'hide_empty' => false,
        'taxonomy' => 'image_categories',
        'parent' => $main_category_id
    ));
    $field_data_id = $_POST['field_data_id'];

    $field_name_subcategorie = 'add_image_data_'.$field_data_id.'_resort_image_sub_category';

    $selected_sub_category = get_field($field_name_subcategorie, $postid)['value'];
    // echo $selected_sub_category;
    $options = '';
    $options .='<option value="none">Select Subcategory</option>';
    foreach ($terms_sub as $choice_sub) {
        $data = '';
        if($selected_sub_category == $choice_sub->term_id){
            $data = "selected";
        }
        $options .='<option value="'.$choice_sub->term_id. '" '.$data.'>' . $choice_sub->name . '</option>';
    }
    // echo $options;
    // new code
    // echo $options;
    $result['html'] = $options;
    $result['catid'] = $selected_sub_category;
    echo json_encode($result);
    // end new code
    die();
}

// new code
add_action('wp_ajax_supSubCategory_by_subCategory', 'supSubCategory_by_subCategory');
add_action('wp_ajax_nopriv_supSubCategory_by_subCategory', 'supSubCategory_by_subCategory');

function supSubCategory_by_subCategory($selected_category) {

    global $post;
    $postid = $post->ID;
    if($postid == ''){
        $postid = $_REQUEST['post'];
    }
    $main_category_id = $_POST['main_category'];

    $terms_sub = get_terms( array(
        'hide_empty' => false,
        'taxonomy' => 'image_categories',
        'parent' => $main_category_id
    ));
    $field_data_id = $_POST['field_data_id'];

    $field_name_subcategorie = 'add_image_data_'.$field_data_id.'_resort_image_super_sub_cat';

    $selected_sub_category = get_field($field_name_subcategorie, $postid)['value'];
    // echo $selected_sub_category;
    $options = '';
    $options .='<option value="none">Select Super Subcategory</option>';
    foreach ($terms_sub as $choice_sub) {
        $data = '';
        if($selected_sub_category == $choice_sub->term_id){
            $data = "selected";
        }
        $options .='<option value="'.$choice_sub->term_id. '" '.$data.'>' . $choice_sub->name . '</option>';
    }
    echo $options;
    die();
}
// end new code


add_action('acf/input/admin_head', 'my_acf_admin_head');

function acf_load_image_category($field) {

    global $post;
    if ($post->post_type == "resort_images"):
        $field['choices'] = array();

        $postid = $post->ID;

         $terms = get_terms( array(
            'hide_empty'        => false,
            'taxonomy' => 'image_categories',
            'parent' => 0
        ) );
         $field['choices'][] = "Select Category";
        foreach ($terms as $choice) {
            $field['choices'][$choice->term_id] = $choice->name;
        }
    endif;
    return $field;
}

add_filter('acf/load_field/name=resort_image_main_category', 'acf_load_image_category');

function acf_load_video_category($field) {

    global $post;
    $terms = '';
    if ($post->post_type == "resort"):
        $field['choices'] = array();

        $postid = $post->ID;

         $terms = get_terms( array(
            'hide_empty'        => false,
            'taxonomy' => 'video_categories',
            'parent' => 0
        ) );
         $field['choices'][] = "Select Category";
        foreach ($terms as $choice) {
            $field['choices'][$choice->term_id] = $choice->name;
        }
    endif;
    return $field;
}

add_filter('acf/load_field/name=resort_video_main_category', 'acf_load_video_category');

function acf_load_marketing_collateral_category($field) {

    global $post;
    $terms = '';
    if ($post->post_type == "resort"):
        $field['choices'] = array();

        $postid = $post->ID;

         $terms = get_terms( array(
            'hide_empty'        => false,
            'taxonomy' => 'marketing_categories',
            'parent' => 0
        ) );
         $field['choices'][] = "Select Category";
        foreach ($terms as $choice) {
            $field['choices'][$choice->term_id] = $choice->name;
        }
    endif;
    return $field;
}

add_filter('acf/load_field/name=resort_marketing_collateral_main_category', 'acf_load_marketing_collateral_category');

function acf_load_logo_category($field) {

    global $post;
    $terms = '';
    if ($post->post_type == "resort"):
        $field['choices'] = array();

        $postid = $post->ID;

         $terms = get_terms( array(
            'hide_empty'        => false,
            'taxonomy' => 'logo_categories',
            'parent' => 0
        ) );
         $field['choices'][] = "Select Category";
        foreach ($terms as $choice) {
            $field['choices'][$choice->term_id] = $choice->name;
        }
    endif;
    return $field;
}

add_filter('acf/load_field/name=resort_logo_main_category', 'acf_load_logo_category');
