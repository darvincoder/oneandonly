<?php

/*
 * Plugin Name: Code implemented By Sagar
 * Description: Custom Coding <strong> (PLEASE DON'T REMOVE THIS PLUGIN)</strong>
 * Author: <strong>Sagar Prajapati </strong> (sagarp4412@gmail.com)
 */

//ini_set('display_errors', 0);

add_action('wp_ajax_newRegsitrations', 'newRegsitrations');
add_action('wp_ajax_nopriv_newRegsitrations', 'newRegsitrations');

function newRegsitrations() {

	global $wpdb;
	$reg_errors = '';
	
	$response = array();	
    $html = '';
	$status = '';
	$username = $_POST['user_name'];
	$user_email = $_POST['user_email'];
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$password = $_POST['password'];
	$user_role = $_POST['user_role'];
	$user_type = $_POST['user_type'];
	$user_space = $_POST['user_space'];

	//Validation	
	if ( empty( $username ) || empty( $password ) || empty( $user_email ) || empty($first_name) || empty($last_name)) {
	    $reg_errors = 'Required form field is missing';
	}
	elseif ( 4 > strlen( $username ) ) {
	    $reg_errors = 'Username too short. At least 4 characters is required';
	}
	elseif ( username_exists( $username ) ) {
    	$reg_errors = 'Sorry, that username already exists!';
	}
	elseif ( 5 > strlen( $password ) ) {
        $reg_errors = 'Password length must be greater than 5';
    }
    elseif ( !is_email( $user_email ) ) {
    	$reg_errors = 'Email is not valid';
	}
	elseif ( email_exists( $user_email ) ) {
	    $reg_errors = 'Email Already in use';
	}

	if($reg_errors == ''){
		$userdata = array(
	        'user_login'    =>   $username,
	        'user_email'    =>   $user_email,
	        'user_pass'     =>   $password,
	        'first_name'    =>   $first_name,
	        'last_name'     =>   $last_name,
	        'nickname'      =>   $first_name,
	        'role' 			=> 	 $user_role
	        );
			$user_id = wp_insert_user( $userdata );
			$u = new WP_User( $user_id );
			$u->add_role( 'public_user' );
			add_user_meta( $user_id, 'user_type', $user_type );
			add_user_meta( $user_id, 'user_space', $user_space  );

			//email activation
			$wpdb->insert('wp_email_activation', array(
			    'user_id' => $user_id,
			    'email' => $user_email,
			    'username' => $username,
			    'token' => '',
			    'status' => 'Yes',
			));

		 # Send email to user
		$body = '<style>';
		$body .= '.normal_text {';
		$body .= 'font-family:Arial, Helvetica, sans-serif;';
		$body .= '  font-size:12px;';
		$body .= '  color:#333333;';
		$body .= '}';
		$body .= '</style>';
		$body .= 'Hello '.$first_name.',<br/><br/>';
		$body .= 'Your Login Details:';
		$body .= '<table bgcolor="#fff" border="0" cellspacing="0" cellpadding="25" width="100%" style="color:#000 !important;">
					<tr>
						<td width="100%" bgcolor="#fff" style="text-align:left !important;color:#000 !important;">																					
								<p style="line-height:10px !important;font-size:16px !important;">
								Username: '.$username.'
								</p>
								<p style="line-height:10px !important;font-size:16px !important;">
								Email: '.$user_email.'
								</p>
								<p style="line-height:10px !important;font-size:16px !important;">
								Password: '.$password.'
								</p>								
						</td>
					</tr>
				</table>';	

		$mail_subject = "Login Details";
		$mail_from = 'no-reply@oneandonlybrand.com';
		$mail_to = $user_email;

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: One&Only <'.$mail_from.'>' . "\r\n";

		if (wp_mail($mail_to, $mail_subject, $body, $headers)):
            $status = "SUCCESS";            
        else:
            $status = "FAIL";
            $reg_errors = 'There is a problem in submitting in your enquiry.';
        endif;
		$status = "SUCCESS";
	}else{
		$status = "FAIL";
	}	

	$response['status'] = $status;
	$response['error'] = $reg_errors;
    //$response['total_events'] = $total_events;
    echo json_encode($response);		

	// wp_reset_postdata();
 	// wp_reset_query();
 	 die();
}

add_action('wp_ajax_newSignin', 'newSignin');
add_action('wp_ajax_nopriv_newSignin', 'newSignin');

function newSignin(){

	//check_ajax_referer( 'ajax-login-nonce', 'security' );

	global $wpdb;
	$username = $wpdb->escape($_REQUEST['login_username']);  
	$password = $wpdb->escape($_REQUEST['login_password']);  
	$remember = "false";  
	      
	if($remember) {
		$remember = "true";  
	}else {
		$remember = "false";  
	}

	//check email is active or not
	$email_active = $wpdb->get_row("SELECT * FROM wp_email_activation WHERE email='".$username."' || username = '".$username."'");
	$new_email = $email_active->email;
	$new_email_status = $email_active->status;

	if($new_email_status == "No"){
		$status = "FAIL";
		$reg_errors = 'Email is not activated! Please confirm your email first.'; 
	}else{
		$login_data = array();  
		$login_data['user_login'] = $username;  
		$login_data['user_password'] = $password;  
		$login_data['remember'] = $remember;  
		$user_verify = wp_signon( $login_data, false );   
		          
		if ( is_wp_error($user_verify) )   
		{  
			$status = "FAIL";
			$reg_errors = 'Invalid username or password. Please try again!'; 
		} else {     
			$status = "SUCCESS"; 
		} 

	}	
	$response['status'] = $status;
	$response['error'] = $reg_errors;

    echo json_encode($response);		

 	 die();  
}

add_action('wp_ajax_newForgetPassword', 'newForgetPassword');
add_action('wp_ajax_nopriv_newForgetPassword', 'newForgetPassword');

function newForgetPassword(){
	global $wpdb;
	$reg_errors = '';	
	$response = array();

	$forget_email = $_POST['forget_email'];

	if ( !is_email( $forget_email ) ) {				
		$status = "FAIL";
    	$reg_errors = 'Email is not valid';
	}
	elseif ( email_exists( $forget_email ) ) {
		$user = get_user_by( 'email', $forget_email );
		$user_id = $user->ID;
		//Generate Radom Password

		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:?";
    	$random_password = substr( str_shuffle( $chars ), 0, 8 );
    	$userdata = array(
				'ID' => $user_id,
		        'user_pass'     =>   $random_password
		        );
		$user_id = wp_update_user($userdata);

    	 # Send email to user
		$body = '<style>';
		$body .= '.normal_text {';
		$body .= 'font-family:Arial, Helvetica, sans-serif;';
		$body .= '  font-size:12px;';
		$body .= '  color:#333333;';
		$body .= '}';
		$body .= '</style>';
		$body .= 'Hello ' . $user->first_name . ' ' . $user->last_name.',<br/><br/>';
		$body .= 'Your New Login Password Details:';
		$body .= '<table bgcolor="#fff" border="0" cellspacing="0" cellpadding="25" width="100%" style="color:#000 !important;">
					<tr>
						<td width="100%" bgcolor="#fff" style="text-align:left !important;color:#000 !important;">																					
								<p style="line-height:10px !important;font-size:16px !important;">
								Username: '.$user->user_login.'
								</p>
								<p style="line-height:10px !important;font-size:16px !important;">
								Email: '.$forget_email.'
								</p>
								<p style="line-height:10px !important;font-size:16px !important;">
								New Password: '.$random_password.'
								</p>								
						</td>
					</tr>
				</table>';	

		$mail_subject = "New Login Details";
		$mail_from = 'no-reply@oneandonlybrand.com';
		$mail_to = $forget_email;

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: One&Only <'.$mail_from.'>' . "\r\n";

		if (wp_mail($mail_to, $mail_subject, $body, $headers)):
            $status = "SUCCESS";            
        else:
            $status = "FAIL";
            $reg_errors = 'There is a problem. Please try again.';
        endif;

	}
	$response['status'] = $status;
	$response['error'] = $reg_errors;
	echo json_encode($response);	
	die();
}

add_action('wp_ajax_updateAccount', 'updateAccount');
add_action('wp_ajax_nopriv_updateAccount', 'updateAccount');

function updateAccount(){

	global $wpdb;
	$reg_errors = '';
	$redirect_logout = '';
	$current_user = wp_get_current_user();

	$response = array();	
    $html = '';
	$status = '';	
	$user_email = $_POST['user_email'];
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$password = $_POST['password'];
	$user_id = $_POST['user_id'];

	if ( $current_user->ID != $_POST['user_id'] ){
		$status = "FAIL";
		$reg_errors = 'Invalid user. Please try again!'; 
	}else{

		if($current_user->user_email != $user_email){
			if ( !is_email( $user_email ) ) {				
				$status = "FAIL";
		    	$reg_errors = 'Email is not valid';
			}
			elseif ( email_exists( $user_email ) ) {
				$status = "FAIL";
			    $reg_errors = 'Email Already in use';
			}else{
				$token = md5(uniqid(rand(), true));

				$wpdb->query("UPDATE wp_email_activation SET status='No', email='".$user_email."', token='".$token."' WHERE user_id='".$user_id."'");

				$subject = 'Confirmation Email'; 
				$message = 'Hello '.$first_name.',';
				$message .= "\n\n";			
				$message .= 'Please click this link to confirm your email address:';
				$message .= home_url('/').'activate.php?token='.$token.'&id='.$user_id;
				$headers = 'From: noreply@test.com' . "\r\n";           
				if(wp_mail($user_email, $subject, $message, $headers)){
					$status = "SUCCESS";
					$reg_errors = 'Confirmation Email has been sent.';
					$userdata = array(
						'ID' => $user_id,
				        'user_email'    =>   $user_email
				        );
					$user_id = wp_update_user($userdata); 
					$redirect_logout = "Yes";
				}else{
					$status = "FAIL";
					$reg_errors = 'Sorry! Something went worng. Please try again!'; 
				}
			}
		}
		if($password != ''){
			if($password != wp_check_password( $pass, $current_user->user_pass, $user_id)){
				$status = "SUCCESS";
				$reg_errors = 'Password has been changed successfilly.';
				$userdata = array(
						'ID' => $user_id,
				        'user_pass'     =>   $password
				        );
				$user_id = wp_update_user($userdata);
				$redirect_logout = "Yes"; 
			}else{
				$status = "FAIL";
				$reg_errors = 'Sorry! Something went worng. Please try again!'; 
			}
		}
		if($current_user->user_pass != $first_name || $current_user->user_pass != $last_name){
			$userdata = array(
			'ID' => $user_id,
	        'first_name'    =>   $first_name,
	        'last_name'     =>   $last_name
	        );
			$user_id = wp_update_user($userdata);
		}

		$response['status'] = $status;
		$response['error'] = $reg_errors;		
		$response['redirect_logout'] = $redirect_logout;
	    
		echo json_encode($response);
		die();  
	}
	
}
