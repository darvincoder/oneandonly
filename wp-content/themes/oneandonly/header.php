<?php
/**
 * The header for our theme
 */
?>
<!DOCTYPE html>

<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title><?php is_front_page() ? bloginfo('name') : wp_title(''); ?></title>
	<meta name="description" content="One and Only Brands">
	<?php
	$favicon = get_field('fevicon_icon','option');
	if($favicon){ ?>
		<link rel="icon" href="<?php echo $favicon['url']; ?>" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo $favicon['url']; ?>" type="image/x-icon" />
		<?php } ?>
		<?php wp_head(); ?>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type="text/javascript">
	   jQuery(document).ready(function() {
	      jQuery('#back-link').click(function(e) {
	        e.preventDefault();
	         history.go(-1)
	       });
	   });
        $(document).ready(function($) {
            var Body = $('body');
            Body.addClass('preloader-site');
            Body.addClass('preloader-site');
        });
        $(window).on('load', function () {
            $('.preloader-wrapper').fadeOut();
            $('body').removeClass('preloader-site');
         });
		</script>
</head>
<?php $page_name = get_the_title(); ?>
<body <?php body_class(); ?>>
<div class="preloader-wrapper">
        <div class="preloader">
                <img src="https://www.tsunami.gov/images/loader.gif" alt="Loader">
        </div>
        <div class="loader_msg">Loading ...</div>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>
    <?php
    if($page_name != "Welcome" && $page_name != "Sign In" && $page_name != "Registration"){ ?>
        <div class="main_container">
            <div class="header_container">
                <div class="nav">
                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <?php get_template_part( 'template-parts/header/header', 'logo' ); ?>
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>
                            <?php if ( has_nav_menu( 'header' ) ) :
                                get_template_part( 'template-parts/navigation/navigation', 'top' );
                             endif; ?>
                            <!--<ul class="nav navbar-nav navbar-right">
                                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                            </ul>-->
                           <?php get_template_part( 'template-parts/header/header', 'search' ); ?>
                        </div>
						<div class="back-btn">
                                        <a href="#" id="back-link" class="breadcrum_btn"><i class="fa fa-angle-left"></i>BACK</a><br/>
                        </div>
                        <div class="breadcrumbs">
                            <?php
                                echo the_breadcrumbs();
                            ?>
                        </div>
                    </nav>
                </div>
            </div>
    <?php }
    ?>
		<?php //echo get_page_template(); ?>
