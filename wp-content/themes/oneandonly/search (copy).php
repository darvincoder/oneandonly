<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">

	<header class="page-header">
		<?php if ( have_posts() ) : ?>
			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyseventeen' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php else : ?>
			<h1 class="page-title"><?php _e( 'Nothing Found', 'twentyseventeen' ); ?></h1>
		<?php endif; ?>
	</header>

	<div class="content_container">
		<div class="content_row row">
			<div class="content_header_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="row col_row_filter">

					<div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">

						<?php
						if ( have_posts() ) :

							while ( have_posts() ) : the_post();

							// get_template_part( 'template-parts/post/content', 'post' ); ?>

							<div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">

								<div class="">

									<?php var_dump(get_permalink()); ?>
									<?php
									$resort_banner_image_arr = get_field('resort_banner_image');
									$resort_banner_image = wp_get_attachment_image_src($resort_banner_image_arr['id'], 'resort_details_banner_image');
									?>

									<img src="<?php echo $resort_banner_image[0]; ?>" alt="" style="width: 100%;"/>

									<div class="image_detail_search">

										<li style="list-style-type:none;">
											<h3><a href="<?php echo get_permalink(); ?>">
												<?php the_title();  ?>
											</a></h3>
											<?php  get_sub_field('resort_image_main_category') ?>
											<h5 style="font-size:14px; white-space:normal;" ><?php  echo substr(get_the_excerpt(), 0,250); ?></h5><br>
											<div class="h-readmore" style="float:left;"> <a href="<?php the_permalink(); ?>">Read More</a></div>
										</li>

									</div>
								</div>

							</div>

							<?php
						endwhile;

						// the_posts_pagination( array(
						// 	'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
						// 	'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
						// 	'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
						// ) );

						else : ?>

						<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
						<?php

					endif;
					?>

				</div>

			</div>
		</div>
	</div>
</div>

<?php get_footer();
