var xhr;
jQuery(function () {  
});

function newSignin(){

    $(".loading").show();
    var login_username = $("#login_username").val();
    var login_password = $("#login_password").val();
    var login_redirect_url = $("#login_home_url").val();
    var security = $("#security").val();
   
    if((login_username == '') || (login_password == '')){
        $("#error_message").show();
        $("#error_message").html("Required form field is missing.");
        $(".loading").hide();
    }else{
        $("#error_message").hide();
        $("#error_message").html("");        

        if (xhr && xhr.readyState != 4 && xhr.readyState != 0) {
            xhr.abort();
        }

        data = {
            action: 'newSignin',
            login_username: login_username,         
            login_password: login_password
        };
        xhr = jQuery.post(ajaxurl, data, function (response) {
            $(".loading").hide();
            var data = JSON.parse(response)
            if(data.status == "FAIL"){
                $("#error_message").show();
                $("#error_message").html(data.error);
            }else{            	
            	window.location = login_redirect_url;                
            }            
        });
    }
}

function newForgetPassword(){
    $(".loading").show();
    var forget_email = $("#forget_email").val();
    if(forget_email == ''){
        $("#error_message").show();
        $("#error_message").html("Please Enter Email Address");
        $(".loading").hide();
    }else{
        $("#error_message").hide();
        $("#error_message").html("");

        if (xhr && xhr.readyState != 4 && xhr.readyState != 0) {
            xhr.abort();
        }
        data = {
            action: 'newForgetPassword',
            forget_email: forget_email                     
        };
        xhr = jQuery.post(ajaxurl, data, function (response) {
            $(".loading").hide();
            var data = JSON.parse(response)
            if(data.status == "FAIL"){
                $("#error_message").show();
                $("#error_message").html(data.error);
            }else{              
                $("#update_thankyou").show();
                $("#forget_password_form").hide();
                $("#login_form").hide();
            }            
        });

    }
}