var xhr;
jQuery(function () {  
});

function updateAccount(){
    $(".loading").show();
	var user_id = $("#user_id").val();
	var first_name = $("#updateFirstname").val();
    var last_name = $("#updateLastname").val();
    var user_name = $("#updateUsername").val();
    var user_email = $("#updateEmail").val();
    var password = $("#updatePassword").val();
    var confirm_password = $("#updateConfirmpassword").val();
    var redirect_url = $("#signin_url").val();

    if(password != confirm_password){
        $("#error_message").show();
        $("#error_message").html("Both password are not macthed.");
        $(".loading").hide();
    }else{
    	$("#error_message").hide();
        $("#error_message").html("");        

        if (xhr && xhr.readyState != 4 && xhr.readyState != 0) {
            xhr.abort();
        }

        data = {
            action: 'updateAccount',
            user_email: user_email,
            first_name: first_name,
            last_name: last_name,
            password: password,
            user_id: user_id,
        };
        xhr = jQuery.post(ajaxurl, data, function (response) {
            $(".loading").hide();
            var data = JSON.parse(response)
            if(data.status == "FAIL"){
                $("#error_message").show();
                $("#error_message").html(data.error);
            }else{ 
                $("#update_account_form").hide();
                $("#update_thankyou").show();

                if(data.redirect_logout == "Yes"){
                    $("#redirect_mesg").show();
                    $("#redirect_home_btn").hide();
                   setInterval(function(){ 
                        //window.location.href = redirect_url;  
                        window.location.href = redirect_url;
                        //jQuery("#signin_url").trigger("click");                      
                       
                    }, 5000);                    
                }else{
                    $("#redirect_mesg").hide();
                    $("#redirect_home_btn").show();
                }               
                

            }
        });
    }

}