$(document).ready(function () {

  getBasketCount();

  $('.add-to-cart').click(function () {

    params = [];
    postid = $(this).data('postid');
    keyword  = $(this).data('keyword');
    type = $(this).data('type');
    let selectedValues = getValueUsingClass();
    // imagekeyword  = $(this).data('imagekeyword');

    var basket_success_msg = document.getElementsByClassName('basket_success_msg');
    var basket_error_msg = document.getElementsByClassName('basket_error_msg');

    if (selectedValues.length < 1) {
      for(var i=0; i<basket_success_msg.length; i++) {
        for(var i=0; i<basket_error_msg.length; i++) {
          basket_success_msg[i].style.display='none';
          basket_error_msg[i].style.display='block';
          $('.basket_error_msg').delay(2000).fadeOut();
        }
      }
    } else {
      for(var i=0; i<basket_success_msg.length; i++) {
        for(var i=0; i<basket_error_msg.length; i++) {
          basket_success_msg[i].style.display='block';
          basket_error_msg[i].style.display='none';
        }
      }
    }

    // console.log(selectedValues);
    data = {
      action: 'addToCart',
      postid: postid,
      selectedValues: selectedValues,
      keyword: keyword,
      type: type,
    };
    $.ajax({
      url: ajaxurl,
      type: 'POST',
      data: data,
      success: function (data) {
        // console.log(data);
        getBasketCount();
        getSingleResortPageCount();
      },
    });


});
});

function getBasketCount() {
  $.ajax({
    url: ajaxurl,
    type: 'POST',
    data: {
      action: 'basketCount',
    },
    success: function (data) {
      if ($('.basket-count span').text() == '') {
        $('.basket-count').append('<span>(' + data + ')</span>');
      } else {
        $('.basket-count span').text('(' + data + ')');
      }
    },
  });
}

function getSingleResortPageCount() {
  postid = $('.add-to-cart').data('postid');
  keyword  = $('.add-to-cart').data('keyword');
  resort_type = $('.add-to-cart').data('type');
  let selectedValues = getValueUsingClass();

  singledata = {
    action: 'singleresortPageCount',
    postid: postid,
    selectedValues: selectedValues,
    keyword: keyword,
    type: type,
  };
  $.ajax({
    url: ajaxurl,
    type: 'POST',
    data: singledata,
    success: function (data) {
      console.log(data);
      data = data.substring(0, data.length - 1);
      if ($('.add-to-cart span').text() == '') {
        $('.add-to-cart').append('<span>' + data + '</span>');
      } else {
        $('.add-to-cart span').text('' + data + '');
      }
    },
  });

}

function getValueUsingClass() {

  var chkArray = [];
  /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
  $('.chk:checked').each(function() {
    chkArray.push($(this).val());
  });

  /* we join the array separated by the comma */
  // var selected;
  // selected = chkArray.join(',');

  // alert(selected);
  // return selected;

  return chkArray;
}
