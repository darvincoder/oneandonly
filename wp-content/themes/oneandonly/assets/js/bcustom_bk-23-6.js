$(document).ready(function () {

  getBasketCount();
  $('.add-to-cart').click(function () {
    params = [];
    postid = $(this).data('postid');
    keyword  = $(this).data('keyword');

    // imagekeyword  = $(this).data('imagekeyword');
    type = $(this).data('type');

    let selectedValues = getValueUsingClass();
    if(selectedValues != '' || null) {
        document.getElementById("basket_success_msg").style.display = "block";
        document.getElementById("basket_error_msg").style.display = "none";
    } else {
      document.getElementById("basket_error_msg").style.display = "block";
      document.getElementById("basket_success_msg").style.display = "none";
    }

    // console.log(selectedValues);
    data = {
      action: 'addToCart',
      postid: postid,
      selectedValues: selectedValues,
      keyword: keyword,
      type: type,
    };
    $.ajax({
      url: ajaxurl,
      type: 'POST',
      data: data,
      success: function (data) {
         // console.log(data);
        getBasketCount();
      },
    });
  });
});

function getBasketCount() {
  $.ajax({
    url: ajaxurl,
    type: 'POST',
    data: {
      action: 'basketCount',
    },
    success: function (data) {
        if ($('.basket-count span').text() == '') {
          $('.basket-count').append('<span>(' + data + ')</span>');
        }else {
          $('.basket-count span').text('(' + data + ')');

        }
      },
  });
}

function getValueUsingClass() {

  var chkArray = [];

  /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
    $('.chk:checked').each(function() {
      chkArray.push($(this).val());
    });

  /* we join the array separated by the comma */
  // var selected;
  // selected = chkArray.join(',');

  // alert(selected);
  // return selected;

  return chkArray;
}

$('button.bsk_msg_close').on('click', function () {
    document.getElementById("basket_success_msg").style.display = "none";
    return false;
});

$(document).ready(function(){
    $("form").submit(function(){
    if ($('input:checkbox').filter(':checked').length < 1){
        document.getElementById("basket_validate_msg").style.display = "block";
        $('#basket_validate_msg').delay(5000).fadeOut();
    return false;
    }
    });
});
