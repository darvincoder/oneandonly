<?php
/**
 * The template for displaying the footer
 */

?>
<?php
$theme_path = get_template_directory_uri();
$page_name = get_the_title();
if($page_name != "Welcome"){ ?>
    <div class="footer_container">
        <div class="footer_header_row row">            
            <div class="footer_header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
            </div>
        </div>

        <div class="footer_content_row row">
            <div class="footer_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="footer_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div>
                        <img src="<?php echo $theme_path; ?>/assets/img/content_image_header.png" class="copy_image">
                    </div>

                    <div class="footer_copyright">
                        &copy 2000 - 2018 One&Only Resorts Ltd.
                    </div>

                     <div class="footer_privacy">
                        <a href="<?php echo get_home_url().'/terms-of-use/'; ?>">TERMS OF USE &nbsp;&nbsp;</a>
                         <a href="<?php echo get_home_url().'/privacy-policy/'; ?>">PRIVACY POLICY</a>
                    </div>
                </div>
            </div>
        </div>   
    </div>  
</div>
<?php } ?>
<?php wp_footer(); ?>
</body>
</html>
   <script>
    // When the user clicks on div, open the popup
    function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");

    $('.container a').click(function(){ 
    var $target = $($(this).data('target')); 
    if(!$target.hasClass('in'))
        $('.container .in').removeClass('in').height(0);
});
    }
    </script>

