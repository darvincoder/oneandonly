<?php
// New Dev Code
function acf_load_post_type_choices( $field ) {

	$field["choices"] = array();
	$choices = get_post_types( array("public" => true) );
	if( is_array($choices) ) {
		foreach( $choices as $choice ) {
			if ($choice == 'post' || $choice == 'page' || $choice == 'attachment' || $choice == 'resort') {
				unset($choices[$choice]);
			}
		}
		foreach( $choices as $choice ) {
			$field["choices"][ $choice ] = get_post_type_object($choice)->label;
		}
	}

	return $field;

}
add_filter("acf/load_field/name=resort_post_type", "acf_load_post_type_choices");
// End new Dev Code
ini_set("allow_url_fopen", true);
if (session_id() == ""){
	session_start();
}


if(!isset($_SESSION['basket'])){
	$_SESSION['basket'] = [];
}

/**
 * Oneandonly functions and definitions
 *
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}


add_action('wp_ajax_addToCart', 'addToCart');
add_action('wp_ajax_nopriv_addToCart', 'addToCart');


add_action('wp_ajax_basketCount', 'basketCount');
add_action('wp_ajax_nopriv_basketCount', 'basketCount');

add_action('wp_ajax_singleresortPageCount', 'singleresortPageCount');
add_action('wp_ajax_nopriv_singleresortPageCount', 'singleresortPageCount');

 add_theme_support('post-thumbnails');
 add_image_size('logo_thumb', 300, 97, false);
 add_image_size('welcome_logo', 290, 94, false);
 add_image_size('page_banner', 1903, 908, false);
 add_image_size('home_content_image', 40, 34, false);
 add_image_size('home_content_brand', 1873, 372, false);
 add_image_size('resort_thumbnail', 600, 450, false);
 add_image_size('resort_image_thumbnail', 600, 450, false);
 add_image_size('resort_video_thumbnail', 600, 450, false);
 add_image_size('resort_marketing_thumbnail', 2438, 329, false);
 add_image_size('resort_logo_thumbnail', 600, 450, false);
 add_image_size('resort_details_banner_image', 1903, 997, false);
// add_image_size('cs_featured', 872, 663, false);
// add_image_size('cs_about', 666, 580, false);
// add_image_size('cs_brief', 1290, 641, false);
// add_image_size('cs_deliverables', 630, 430, false);
// add_image_size('client_thumb', 220, 109, false);

global $passWordCat;
$GLOBALS['passWordCat'] = [263,265,51,164,165];
function removePasswordData($data)
{
	foreach ($data as $key => $value) {
		if (in_array($value['resort_image_main_category']['value'], $GLOBALS['passWordCat']) ||
			in_array($value['resort_image_sub_category']['value'], $GLOBALS['passWordCat']) ||
			in_array($value['resort_image_super_sub_cat']['value'], $GLOBALS['passWordCat'])) {
			unset($data[$key]);
		}
	}
	return $data;
}

function addToCart() {
// dd($_POST['selectedValues']);
	// $product = array(
	// 	'postid' => $_POST['postid'],
	// 	'type' => $_POST['type']
	// );
	// session_start();
	// $basket = $_SESSION['basket'];
	// $_SESSION['basket'] = ['132'];
	// var_dump($_SESSION['basket']);
	// die();


// $existPost = getExistpost($product);
// 	if ($existPost['exist']) {
// 		$basket[$existPost['basketKey']][$_POST['type']] = $product[$_POST['type']];
// 		$_SESSION['basket'] = $basket;
// 		$_SESSION['imageAdd'] = $_POST['keyword'];
// 		var_dump('it is already exists');
// 		session_write_close();
// 	}else {
// 		array_push($basket,$product);
// 		$_SESSION['basket'] = $basket;
// 		$_SESSION['imageAdd'] = $_POST['postid'];
// 		session_write_close();
// 	}
	// echo json_encode($_SESSION['basket']);

	die();
}

function getExistpost($product, $result=[]) {
	foreach ($basket as $key => $item) {
		if($item['postid'] == $product['postid'] && $item['type'] == $product['type']){
				$result['basketKey'] = $key;
				$result['exist'] = true;
				return $result;
			}
	}
	$result['exist'] = false;
	return $result;
}


function basketCount() {
session_start();
	$basket = $_SESSION['basket'];
	$count = 0;
	foreach ($basket as $resort) {
		if(array_key_exists('resort_images', $resort)){
			$count += count($resort['resort_images']);
		}
		if(array_key_exists('video', $resort)){
			$count += count($resort['video']);
		}
		if(array_key_exists('marketing', $resort)){
			$count += count($resort['marketing']);
		}
		if(array_key_exists('logo', $resort)){
			$count += count($resort['logo']);
		}

	}
	echo $count;
 die();

}


 add_action('wp_enqueue_scripts', 'theme_enqueue_styles');


 function theme_enqueue_styles() {
 	global $post;
    $postid = $post->ID;
 	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.css');
 	wp_enqueue_style('bootstrap-theme', get_stylesheet_directory_uri() . '/assets/css/bootstrap-theme.min.css');
    wp_enqueue_style('main-style', get_stylesheet_directory_uri() . '/assets/css/style.css');
	wp_enqueue_style('font-awasome', get_stylesheet_directory_uri() . '/assets/css/font-awesome.min.css');

	wp_enqueue_script('jquery-js', get_stylesheet_directory_uri() . '/assets/js/jquery.min.js', false);
    wp_enqueue_script('tether-js', get_stylesheet_directory_uri() . '/assets/js/tether.min.js', false);
	wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', false);
	wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', false);

	if (is_page('registration')):
        wp_enqueue_script('register-js', get_stylesheet_directory_uri() . '/assets/js/register.js', false);
        wp_localize_script('register-js', 'ajaxurl', admin_url('admin-ajax.php'));
    endif;

    if (is_page('login')):
        wp_enqueue_script('signin-js', get_stylesheet_directory_uri() . '/assets/js/signin.js', false);
        wp_localize_script('signin-js', 'ajaxurl', admin_url('admin-ajax.php'));
    endif;

    if (is_page('myaccount')):
        wp_enqueue_script('myaccount-js', get_stylesheet_directory_uri() . '/assets/js/myaccount.js', false);
        wp_localize_script('myaccount-js', 'ajaxurl', admin_url('admin-ajax.php'));
  endif;

	wp_enqueue_script('register-js', get_stylesheet_directory_uri() . '/assets/js/bcustom.js', false);
	wp_localize_script('register-js', 'ajaxurl', admin_url('admin-ajax.php'));
}
function theme_enqueue_scripts() {
 //    wp_enqueue_script('jquery-js', get_stylesheet_directory_uri() . '/assets/js/jquery.min.js', false);
 //    wp_enqueue_script('tether-js', get_stylesheet_directory_uri() . '/assets/js/tether.min.js', false);
	// wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', false);
	// wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', false);

}

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

add_action( 'wp_footer', 'theme_enqueue_scripts' );
add_action( 'wp_print_scripts', 'de_script', 100 );

function de_script() {
    wp_dequeue_script( 'jquery' );
    wp_deregister_script( 'jquery' );
}

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
	if (current_user_can('public_user')) {
	  show_admin_bar(false);
	}
}

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );

function wti_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'login') {
      if (is_user_logged_in()) {
         $items .= '<li class="right"><a href="'. wp_logout_url(get_home_url().'/signout/') .'">'. __("Sign Out") .'</a></li>';
      }
   }
   return $items;
}

function twentyseventeen_setup() {


	load_theme_textdomain( 'twentyseventeen' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );
	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );

	$GLOBALS['content_width'] = 525;

	register_nav_menus( array(
		'header' => __( 'Header Menu', 'twentyseventeen' ),
		'footer'  => __( 'Footer Menu', 'twentyseventeen' ),
		'login'  => __( 'Login Menu', 'twentyseventeen' ),
	) );

	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	add_theme_support( 'customize-selective-refresh-widgets' );

	add_editor_style( array( 'assets/css/editor-style.css', twentyseventeen_fonts_url() ) );

	$starter_content = array(
		'widgets' => array(
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			'sidebar-2' => array(
				'text_business_info',
			),

			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		'nav_menus' => array(

			'top' => array(
				'name' => __( 'Top Menu', 'twentyseventeen' ),
				'items' => array(
					'link_home',
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			'social' => array(
				'name' => __( 'Social Links Menu', 'twentyseventeen' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	$starter_content = apply_filters( 'twentyseventeen_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'twentyseventeen_setup' );

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Website General Settings',
        'menu_title' => 'Website Settings',
        'menu_slug' => 'website-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

$user_info = get_userdata(get_current_user_id());
//print_r($user_info); exit();
if(!empty($user_info)){
	$user_role = $user_info->roles[0];

	if($user_role == "editor"){
		// Removes from admin menu
		add_action( 'admin_menu', 'my_remove_admin_menus' );
		function my_remove_admin_menus() {
		    remove_menu_page( 'edit-comments.php' );
		    remove_menu_page( 'tools.php' );
		    remove_menu_page( 'edit.php' );
		}
	}
}

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
	if (current_user_can('public_user')) {
	  wp_redirect(get_home_url().'/oneandonly-brand/');
	}
	//exit();
}

function twentyseventeen_content_width() {

	$content_width = $GLOBALS['content_width'];

	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( twentyseventeen_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	$GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );
}
add_action( 'template_redirect', 'twentyseventeen_content_width', 0 );

function twentyseventeen_fonts_url() {
	$fonts_url = '';

	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'twentyseventeen' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

function twentyseventeen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );

function twentyseventeen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'twentyseventeen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'twentyseventeen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'twentyseventeen' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyseventeen_widgets_init' );

function twentyseventeen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'twentyseventeen_excerpt_more' );

function twentyseventeen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );


function twentyseventeen_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'twentyseventeen_pingback_header' );

function twentyseventeen_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo twentyseventeen_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'twentyseventeen_colors_css_wrap' );

function twentyseventeen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2 );

function twentyseventeen_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );

function twentyseventeen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3 );

function twentyseventeen_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'twentyseventeen_front_page_template' );

require get_parent_theme_file_path( '/inc/custom-header.php' );

require get_parent_theme_file_path( '/inc/template-tags.php' );

require get_parent_theme_file_path( '/inc/template-functions.php' );

require get_parent_theme_file_path( '/inc/customizer.php' );

require get_parent_theme_file_path( '/inc/icon-functions.php' );

/* Get Image by Keyword */

function getFileByKeyword($keyword, $postid, $type, $fileAcfName, $result=[]) {
	if ($type == 'image') {
		$images = get_field('add_image_data',$postid);
		foreach ($images as $image) {
			if($keyword == $image['image_keywords']){
				$result['url'] = $image[$fileAcfName]['url'];
				$result['name'] = $image['image_name'];
				return $result;
			}
		}
	}
	if ($type == 'video') {
		$videos = get_field('add_video_data',$postid);
		foreach ($videos as $video) {
			if($keyword == $video['video_keyword']){
				$result['url'] = $video['add_low_resolution_video_360p']['url'];
				$result['name'] = $video['video_name'];
				return $result;
			}
		}
	}
	if ($type == 'marketing') {
		$marketings = get_field('add_marketing_collateral',$postid);
		foreach ($marketings as $marketing) {
			if($keyword == $marketing['marketing_collateral_keywords']){
				$result['url'] = $marketing['add_marketing_collateral_file']['url'];
				$result['name'] = $marketing['marketing_collateral_name'];
				return $result;
			}
		}
	}
	if ($type == 'logo') {
		$logos = get_field('add_logo_data',$postid);
		foreach ($logos as $logo) {
			if($keyword == $logo['logo_keywords']){
				$result['url'] = $logo['add_low_resolution_logo_print_72kb']['url'];
				$result['name'] = $logo['logo_name'];
				return $result;
			}
		}
	}
}

function resortPageCount($keyword, $postid, $resort_type){
	// session_start();
	$basket = $_SESSION['basket'];
	$count = 0;
	foreach ($basket as $resort) {
		if($resort['postid'] == $postid && $resort['keyword'] == $keyword && array_key_exists($resort_type, $resort)){
		 $count = count($resort[$resort_type]);
		 return $count;
		 echo $count;
		}
	}
	return $count;
}



function singleresortPageCount(){
	$postid = $_POST['postid'];
	$keyword = $_POST['keyword'];
	$resort_type = $_POST['type'];

	$basket = $_SESSION['basket'];
	$count = 0;
	foreach ($basket as $resort) {
		if($resort['postid'] == $postid && $resort['keyword'] == $keyword && array_key_exists($resort_type, $resort)){
			echo count($resort[$resort_type]);
		 $count = count($resort[$resort_type]);
		}
	}
}

add_action( 'admin_post_single_resort_image_download_assets', 'single_resort_image_download_assets' );
add_action( 'admin_post_single_resort_download_assets', 'single_resort_download_assets' );
add_action( 'admin_post_bulk_download_assets', 'bulk_download_assets' );
add_action( 'admin_post_basket_bulk_download_assets', 'basket_bulk_download_assets' );

//this next action version allows users not logged in to submit requests

//if you want to have both logged in and not logged in users submitting, you have to add both actions!

// add_action( 'admin_post_nopriv_download_assets', 'download_assets' );

function basket_bulk_download_assets()
{
	// dd($_REQUEST['basketallimages']);
	$files = [];
	$allimages = json_decode(stripslashes($_REQUEST['basketallimages']));
	if( isset($allimages) && !empty($allimages) && !isset($allassets)){
		// dd($allimages);
		foreach ($allimages as $allimage) {
			download_history( get_current_user_id(), $allimage->postid, $allimage->keyword, $allimage->files, $allimage->type);
			$files = array_merge($files, $allimage->files);
		}
		zipBasketDownload($files);
	}
	$allvideos = json_decode(stripslashes($_REQUEST['basketallvideos']));
	if( isset($allvideos) && !empty($allvideos) && !isset($allassets)){
		// dd($allimages);
		foreach ($allvideos as $allvideo) {
			download_history( get_current_user_id(), $allvideo->postid, $allvideo->keyword, $allvideo->files, $allvideo->type);
			$files = array_merge($files, $allvideo->files);
		}
		zipBasketDownload($files);
	}
	$allmarketing = json_decode(stripslashes($_REQUEST['basketallmarketing']));
	if( isset($allmarketing) && !empty($allmarketing) && !isset($allassets)){
		// dd($allimages);
		foreach ($allmarketing as $marketing) {
			download_history( get_current_user_id(), $marketing->postid, $marketing->keyword, $marketing->files, $marketing->type);
			$files = array_merge($files, $marketing->files);
		}
		zipBasketDownload($files);
	}
	$alllogos = json_decode(stripslashes($_REQUEST['basketalllogos']));
	if( isset($alllogos) && !empty($alllogos) && !isset($allassets)){
		// dd($allimages);
		foreach ($alllogos as $alllogo) {
			download_history( get_current_user_id(), $alllogo->postid, $alllogo->keyword, $alllogo->files, $alllogo->type);
			$files = array_merge($files, $alllogo->files);
		}
		zipBasketDownload($files);
	}
	$allAssets = json_decode(stripslashes($_REQUEST['basketallassets']));
	if( isset($allAssets) && !empty($allAssets)){
		foreach ($allAssets as $allasset) {
			download_history( get_current_user_id(), $allasset->postid, $allasset->keyword, $allasset->files, $allasset->type);
			$files = array_merge($files, $allasset->files);
		}
		zipBasketDownload($files);
	}
	die();
}


function bulk_download_assets() {
	$allimages = json_decode(stripslashes($_REQUEST['allimages']));
	// echo json_last_error();
	// dd(json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $_REQUEST['allimages']), true ));
	// dd($allimages);
	$allvideos = json_decode(stripslashes($_REQUEST['allvideos']));
	$allmarketing = json_decode(stripslashes($_REQUEST['allmarketing']));
	$alllogos = json_decode(stripslashes($_REQUEST['alllogos']));
	$allassets = json_decode(stripslashes($_REQUEST['allassets']));
	if( isset($allimages) && !empty($allimages) && !isset($allassets)){
		zipBasketDownload($allimages);
	}
	if( isset($allvideos) && !empty($allvideos) && !isset($allassets)){
		zipBasketDownload($allvideos);
	}
	if( isset($alllogos) && !empty($alllogos) && !isset($allassets)){
		zipBasketDownload($alllogos);
	}
	if( isset($allmarketing) && !empty($allmarketing) && !isset($allassets)){
		zipBasketDownload($allmarketing);
	}
	if (isset($allassets) && !empty($allassets)) {
		zipBasketDownload($allassets);
	}
}

function single_resort_image_download_assets() {
	$resort_url = $_REQUEST['image_url'];
	$resort_name = $_REQUEST['resort_name'];
	$resort_keyword = $_REQUEST['keyword'];
	$basket = $_SESSION['basket'];

	if (!empty(basket_image_check($_REQUEST))){
		$images = basket_image_check($_REQUEST);
		zipDownload($images, $_REQUEST['resort_name']);
		download_history( get_current_user_id(), $_REQUEST['postid'], $_REQUEST['keyword'], $images, $_REQUEST['type']);

	} else {
		$_SESSION["noImageSelect"] = $resort_keyword;
		$noImageSelect =  $_SESSION["noImageSelect"];
		header("Location: $resort_url" );
	}
}

function basket_image_check($request) {
		$resort_url = $request['image_url'];
		$resort_name = $request['resort_name'];
		$resort_keyword = $request['keyword'];
		$basket = $_SESSION['basket'];
		$images = [];

		foreach($basket as $value){
			$basket_keyword = $value['keyword'];
			if($basket_keyword === $resort_keyword){
				$images = $value['resort_images'];
				return $images;
			}
    }
		return $images;
	}

function single_resort_download_assets() {
	$resort_name = $_REQUEST['resort_name'];
	if( isset($_REQUEST['images']) && !empty($_REQUEST['images']) ){
		// dd($_REQUEST['resort_name']);
		zipDownload($_REQUEST['images'], $resort_name);
		download_history( get_current_user_id(), $_REQUEST['postid'], $_REQUEST['keyword'], $_REQUEST['images'], $_REQUEST['type']);
	}
	if( isset($_REQUEST['videos']) && !empty($_REQUEST['videos']) ) {


		zipDownload($_REQUEST['videos'], $resort_name);
		download_history( get_current_user_id(), $_REQUEST['postid'], $_REQUEST['keyword'], $_REQUEST['videos'], $_REQUEST['type']);
	}
	if( isset($_REQUEST['marketing']) && !empty($_REQUEST['marketing']) ) {
		zipDownload($_REQUEST['marketing'], $resort_name);
		download_history( get_current_user_id(), $_REQUEST['postid'], $_REQUEST['keyword'], $_REQUEST['marketing'], $_REQUEST['type']);
	}
	if( isset($_REQUEST['logos']) && !empty($_REQUEST['logos']) ) {
		zipDownload($_REQUEST['logos'], $resort_name);
		download_history( get_current_user_id(), $_REQUEST['postid'], $_REQUEST['keyword'], $_REQUEST['logos'], $_REQUEST['type']);
	}
	die();
}
/**
* $files type array
* return file download start automatic
*/

function zipBasketDownload($files)
{
	ini_set('memory_limit','2048M');
	$zip = new ZipArchive;
	$zipName = 'OneandOnly_Basket_'.date("d_m_Y").'_'.get_current_user_id().'.zip';
	$filename = get_template_directory().'/downloads/'.$zipName;
	// die($filename);
	if ($zip->open($filename, ZipArchive::CREATE) === TRUE)
	{
		foreach ($files as $file) {
			$zip->addFromString(basename($file),file_get_contents($file));
		}
		$zip->close();
	}
	header("Content-type: application/zip");
	header("Content-Disposition: attachment; filename=" . $zipName);
	header("Pragma: no-cache");
	header("Expires: 0");
	readfile($filename);
	unlink($filename);
}

function zipDownload($files, $resort_name)
{
	// var_dump($files);
	// var_dump($resort_name);
	// die();
	ini_set('memory_limit','20048M');
	$zip = new ZipArchive;
	$zipName = $resort_name.'_'.date("d_m_Y").'_'.get_current_user_id().'.zip';
	$filename = get_template_directory().'/downloads/'.$zipName;
	// die($filename);
	if ($zip->open($filename, ZipArchive::CREATE) === TRUE)
	{
		foreach ($files as $file) {
			//$file = 'http://staging.oneandonlybrand.com/wp-content/uploads/2018/03/OneAndOnly_RoyalMirage_Accommodation_AC_DeluxeRoomDetail_LR.jpg';
			$zip->addFromString(basename($file),file_get_contents($file, FILE_USE_INCLUDE_PATH));
		}
		$zip->close();
	}
	header("Content-type: application/zip");
	header("Content-Disposition: attachment; filename=" . $zipName);
	header("Pragma: no-cache");
	header("Expires: 0");
	readfile($filename);
	unlink($filename);
}

function download_history($userId, $postId, $keyword, $filesArray, $type) {
	// $userId = 1;
	// $postId = 221;
	// $imageKeyword = 'img_img';
	// $imagesArray = ["qwe","asdf"];
	$files = json_encode($filesArray);
	$date = date("Y-m-d");
  // $type = 'image';
	global $wpdb;
	$checkQuery = "SELECT id FROM wp_download_history WHERE user_id=".$userId." AND post_id=".$postId." AND keyword='".$keyword."'";
	$checkresult = $wpdb->get_results($checkQuery);
	if (!empty($checkresult)) {
		$updateRow = $checkresult[0]->id;
		$wpdb->query( $wpdb->prepare("UPDATE wp_download_history SET files=%s WHERE id=%d", [$files,$updateRow]) );
	}else{
		$wpdb->query( $wpdb->prepare("INSERT INTO `wp_download_history`(`user_id`, `post_id`, `date`, `keyword`, `files`, `type`) VALUES (%d, %d, %s, %s, %s, %s)",[$userId,$postId,$date,$keyword,$files,$type]) );
	}
	// die();
}
// healper functions
function getAcfFileDate($file){
	return date("d-m-Y",strtotime($file['date']));
}
function dd($value='')
{
	echo "<pre>";
	var_dump($value);
	die();
}

function shortRepiterDataByCategory($repiterFielName,$postId,$categoryFieldKey,$categoryId) {
	$result = [];
	if(get_field($repiterFielName,$postId)){
		// dd($categoryFieldKey);
		foreach (get_field($repiterFielName,$postId) as $field) {
			if (is_array($field[$categoryFieldKey]) && $field[$categoryFieldKey]["value"] == $categoryId) {
				array_push($result, $field);
			}
		}
	}
	return $result;
}

function getImageNoneEmptyCat($postId)
{
	$catIdArray = [];
	$images = get_field('add_image_data',$postId);
	if ($images) {
		foreach ($images as $imagekey => $image) {
			foreach ($image as $key => $value) {
					if(isset($value['value']) && $value['value'] != 0 && $value['value'] != 'none') {
						if ($key == "resort_image_main_category") {
							array_push($catIdArray, $value['value']);
						}
						elseif ($key == "resort_image_sub_category") {
							array_push($catIdArray, $value['value']);
						}elseif ($key == "resort_image_super_sub_cat") {
							array_push($catIdArray, $value['value']);
						}
					}
			}
		}
	}
	return array_unique($catIdArray);
}

function getVideoNoneEmptyCat($postId)
{
	$catIdArray = [];
	$videos = get_field('add_video_data',$postId);
	if ($videos) {
		foreach ($videos as $videokey => $video) {
			foreach ($video as $key => $value) {
					if(isset($value['value']) && $value['value'] != 0 && $value['value'] != 'none') {
						if ($key == "resort_video_main_category") {
							array_push($catIdArray, $value['value']);
						}
					}
			}
		}
	}
	return array_unique($catIdArray);
}

function getMarketingNoneEmptyCat($postId)
{
	$catIdArray = [];
	$marketings = get_field('add_marketing_collateral',$postId);
	if ($marketings) {
		foreach ($marketings as $marketingkey => $marketing) {
			foreach ($marketing as $key => $value) {
					if(isset($value['value']) && $value['value'] != 0 && $value['value'] != 'none') {
						if ($key == "resort_marketing_collateral_main_category") {
							array_push($catIdArray, $value['value']);
						}
					}
			}
		}
	}
	return array_unique($catIdArray);
}

function getLogoNoneEmptyCat($postId)
{
	$catIdArray = [];
	$logos = get_field('add_logo_data',$postId);
	if ($logos) {
		foreach ($logos as $logokey => $logo) {
			foreach ($logo as $key => $value) {
					if(isset($value['value']) && $value['value'] != 0 && $value['value'] != 'none') {
						if ($key == "resort_logo_main_category") {
							array_push($catIdArray, $value['value']);
						}
					}
			}
		}
	}
	return array_unique($catIdArray);
}

function my_enqueue($hook) {
    // Only add to the edit.php admin page.
    // See WP docs.
    if ('post.php' !== $hook) {
        return;
    }
    wp_enqueue_script('loader_script', get_template_directory_uri() . '/assets/js/loaderscript.js');
		wp_enqueue_style( 'loader_style', get_template_directory_uri() . '/assets/css/loaderstyle.css' );
}

add_action('admin_enqueue_scripts', 'my_enqueue');
  if ( is_page( 'resort-images' )) { console.log('kp');?>
    <script src="gmap.js"></script>
  <?php
}

// BreadCrump Code

function the_breadcrumbs() {
	global $post;
    if (is_category() || is_single()) {
        echo "";
        $cats = get_the_category( $post->ID );
        foreach ( $cats as $cat ){
            echo $cat->cat_name;
            echo "";
        }
        if (is_single()) {
    	    echo "<a href='";
        	echo the_permalink(125);
           	echo "'>";
           	echo "Home";
           	echo "</a> &nbsp; > &nbsp; "; echo "<a href='";the_permalink();echo "'>";the_title();echo "</a> &nbsp; ";
        }
    }
    elseif (is_page("resort-home")) {
    	echo "<a href='";
    	echo the_permalink(125);
    	echo "'>";
        echo "Home";
        echo "</a> &nbsp; &nbsp; ";
    }
    elseif (is_page()) {
        if($post->post_parent){
        $anc = get_post_ancestors( $post->ID );
       	$anc_link = get_page_link( $post->post_parent );
        	foreach ( $anc as $ancestor ) {
            	$output =  "<a href='";
       			echo the_permalink(125);
       			echo "'>";
       			echo "Home";
       			echo "</a> &nbsp; > &nbsp; "; "<a href=".$anc_link.">".get_the_title($ancestor)."</a>";
        	}
       		echo "<a href='";
       		echo the_permalink(125);
       		echo "'>";
        	echo "Home";
       	 	echo "</a> &nbsp; > &nbsp; ";
        	the_title();
        } else {
              	echo "<a href='";
             	echo the_permalink(125);
             	echo "'>";
             	echo "Home";
             	echo "</a> &nbsp; > &nbsp; ";
				echo "<a href='";echo the_permalink();echo "'>";echo the_title();echo "</a> &nbsp; ";
           		echo ' ';
            }
        }
    	elseif (is_tag()) {echo "<a href='";
            echo the_permalink(125);
            echo "'>";
            echo "Home";
            echo "</a> &nbsp; > &nbsp; ";single_tag_title();}
    	elseif (is_day()) {echo"Archive: "; echo "<a href='";
            echo the_permalink(125);
            echo "'>";
            echo "</a> &nbsp; > &nbsp; ";the_time('F jS, Y');}
    	elseif (is_month()) {echo"Archive: "; echo "<a href='";
            echo the_permalink(125);
            echo "'>";
            echo "Home";
            echo "</a> &nbsp; > &nbsp; "; the_time('F, Y');}
    	elseif (is_year()) {echo"Archive: "; echo "<a href='";
            echo the_permalink(125);
            echo "'>";
            echo "Home";
            echo "</a> &nbsp; > &nbsp; "; the_time('Y');}
    	elseif (is_author()) {echo"Author's archive: ";}
    	elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<a href='";
            echo the_permalink(125);
            echo "'>";
            echo "Home";
            echo "</a> &nbsp; > &nbsp; ";"Blogarchive: ";}
    	elseif (is_search()) {echo"Search results: "; }
    	if($_REQUEST['slug']){
    		global $post;
			$theme_path = get_template_directory_uri();
			$resort_type = '';
			$CurrentUrl = get_permalink();
			$image_display = $video_display = $marketing_display = $logo_display = "style='display:none';";
			$resort_type = get_queried_object()->post_type;
			$resort_value = $_REQUEST['slug'];

			if(isset($resort_value) && $resort_value != '' && $resort_type != ''){
		  		if($resort_type == "resort_images"){
		    		$resort_type_value = "IMAGE";
		    		$image_display = "style='display:block';";
		    		$repiter_field = "add_image_data";
		    		$keyword_field = "image_keywords";
		    		$name_field    = "image_name";
		    		$description_field = "image_description";
		  		}elseif($resort_type == "video"){
		    		$resort_type_value = "VIDEO";
		    		$video_display = "style='display:block';";
		    		$repiter_field = "add_video_data";
		    		$keyword_field = "video_keyword";
		    		$name_field    = "video_name";
		  		}elseif($resort_type == "marketing"){
		    		$resort_type_value = "MARKETING COLLATERAL";
		    		$marketing_display = "style='display:block';";
		    		$repiter_field = "add_marketing_collateral";
		    		$keyword_field = "marketing_collateral_keywords";
		    		$name_field    = "marketing_collateral_name";
		  		}elseif($resort_type == "logo"){
		    		$resort_type_value = "LOGOS & MOTIFS";
		    		$logo_display = "style='display:block';";
		    		$repiter_field = "add_logo_data";
		    		$keyword_field = "logo_keywords";
		    		$name_field    = "logo_name";
		  		}
			}
			if($_REQUEST['slug']){
				$resort_type = '';
				$CurrentUrl = get_permalink();
				$image_display = $video_display = $marketing_display = $logo_display = "style='display:none';";
				$resort_type = $_REQUEST['type'];
			if(isset($resort_type) && $resort_type != ''){
  				if($resort_type == "image"){
    				$resort_type_value = "IMAGES";
    				$repiter_field = "add_image_data";
    				$keyword_field = "image_keywords";
    				$name_field    = "image_name";
  				}elseif($resort_type == "video"){
    				$resort_type_value = "VIDEO";
    				$repiter_field = "add_video_data";
    				$keyword_field = "video_keyword";
    				$name_field    = "video_name";
			  	}elseif($resort_type == "marketing"){
			    	$resort_type_value = "MARKETING COLLATERAL";
			    	$repiter_field = "add_marketing_collateral";
			    	$keyword_field = "marketing_collateral_keywords";
			    	$name_field    = "marketing_collateral_name";
  				}elseif($resort_type == "logo"){
    				$resort_type_value = "LOGOS & MOTIFS";
    				$repiter_field = "add_logo_data";
    				$keyword_field = "logo_keywords";
    				$name_field    = "logo_name";
  				}
			}
			if(isset($resort_value) && $resort_value != '' && $resort_type != ''){
		  	if($resort_type == "image"){
		    	$resort_type_value = "IMAGE";
		    	$repiter_field = "add_image_data";
		    	$keyword_field = "image_keywords";
		    	$name_field    = "image_name";
		    	$description_field = "image_description";
		  	}elseif($resort_type == "video"){
		    	$resort_type_value = "VIDEO";
		    	$repiter_field = "add_video_data";
		    	$keyword_field = "video_keyword";
		    	$name_field    = "video_name";
		    	$description_field = "video_description";
		  	}elseif($resort_type == "marketing"){
		    	$resort_type_value = "MARKETING COLLATERAL";
		    	$repiter_field = "add_marketing_collateral";
		    	$keyword_field = "marketing_collateral_keywords";
		    	$name_field    = "marketing_collateral_name";
		    	$marketing_resort_file = "add_marketing_collateral_file";
		    	$description_field = "marketing_collateral_description";
		  	}elseif($resort_type == "logo"){
		    	$resort_type_value = "LOGOS & MOTIFS";
		    	$repiter_field = "add_logo_data";
		    	$keyword_field = "logo_keywords";
		    	$name_field    = "logo_name";
		    	$description_field = "logo_description";
		  	}
		}
  	}
	if( have_rows($repiter_field) ):
		while ( have_rows($repiter_field) ) : the_row();
			if(get_sub_field($keyword_field) == $_GET['slug']) :
				echo " &nbsp; > &nbsp; " . get_sub_field($name_field);
			endif;
		endwhile;
	endif;
  }
}
