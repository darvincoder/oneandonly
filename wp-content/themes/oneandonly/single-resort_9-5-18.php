<?php

get_header();
$theme_path = get_template_directory_uri();

$page_type = $_REQUEST['type'];
$type_slug = @$_REQUEST['slug'];

if(isset($page_type) && $page_type != '' && $type_slug == ''){
    get_template_part( 'template-parts/page/content', 'front-resort-type' );
}elseif(isset($type_slug) && $type_slug != '' && $page_type != ''){
    get_template_part( 'template-parts/page/content', 'front-resort-slug' );
}else{
    while ( have_posts() ) : the_post(); ?>
        <?php
        $resort_banner_image_arr = get_field('resort_banner_image');
        $resort_banner_image = wp_get_attachment_image_src($resort_banner_image_arr['id'], 'resort_details_banner_image');
        ?>
        <div class="slider_container">
            <div class="slider_content_row row">
                <div class="slider_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="<?php echo $resort_banner_image[0]; ?>" alt="" style="width: 100%;" />
                    <!--<div class="slider">
                        <img src="img/nature1.jpg" class="slider_image" alt="" />
                        <img src="img/nature1.jpg" class="slider_image" alt="" />
                        <img src="img/nature1.jpg" class="slider_image" alt="" />
                    </div> -->
                </div>
            </div>
        </div>
        <?php
        $resort_title = get_field('resort_banner_title');
        $resort_short_name = explode(",", $resort_title);
        $resort_description = get_the_content();

		$resort_image_title = get_field('resort_image_thumbnail_title');
		$resort_image_thumbnail_arr = get_field('resort_image_thumbnail');
		$resort_image_thumbnail = wp_get_attachment_image_src($resort_image_thumbnail_arr['id'], 'resort_image_thumbnail');

		$resort_video_title = get_field('resort_video_thumbnail_title');
		$resort_video_thumbnail_arr = get_field('resort_video_thumbnail');
		$resort_video_thumbnail = wp_get_attachment_image_src($resort_video_thumbnail_arr['id'], 'resort_video_thumbnail');

		$resort_marketing_title = get_field('resort_marketing_collateral_thumbnail_title');
		$resort_marketing_thumbnail_arr = get_field('resort_marketing_collateral_thumbnail');
		$resort_marketing_thumbnail = wp_get_attachment_image_src($resort_marketing_thumbnail_arr['id'], 'resort_marketing_thumbnail');

		$resort_logo_title = get_field('resort_logo_thumbnail_title');
		$resort_logo_thumbnail_arr = get_field('resort_logo_thumbnail');
		$resort_logo_thumbnail = wp_get_attachment_image_src($resort_logo_thumbnail_arr['id'], 'resort_logo_thumbnail');

		?>
		<div class="content_container">
		    <div class="content_row row">
		        <div class="content_header_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h1><?php echo strtoupper($resort_title); ?></h1>
		                <p class="extraPadding">
		                	<?php echo $resort_description ?>
		                </p>
		        </div>
		    </div>
    		<div class="content_row row">
        		<div class="content_wrapper_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            		<div class="content_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="content_row row">

                            <?php
                            if($resort_image_thumbnail[0] != '' && $resort_image_title != ''){ ?>
                                <div class="content_images_about col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="inner_image col-lg-12 col-md-12 col-sm-12 col-xs-12"">
                                        <?php
                                        // dd(get_permalink(get_field('select_resort_images')));
                                        // $ImageListingUrl = get_permalink();
                                        $NewImageListingUrl = get_permalink(get_field('select_resort_images'));
                                        ?>
                                        <a href="<?php echo $NewImageListingUrl; ?>">
                                            <img src="<?php echo $resort_image_thumbnail[0]; ?>" alt="Resort Image"  class="image" style="width:100%;">
                                            <div class="overlay"></div>
                                            <div class="text"><?php echo $resort_image_title; ?></div>
                                        </a>
                                    </div>
                                </div>
                            <?php
                            }

                            if($resort_video_thumbnail[0] != '' && $resort_video_title != ''){ ?>
                                <div class="content_images_about col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="inner_image col-lg-12 col-md-12 col-sm-12 col-xs-12"">
                                        <?php
                                        $VideoListingUrl = get_permalink();
                                        $NewVideoListingUrl = rtrim($VideoListingUrl,"/").'?type=video';
                                        ?>
                                        <a href="<?php echo $NewVideoListingUrl; ?>"><img src="<?php echo $resort_video_thumbnail[0];?>" alt="Resort Video" class="image" style="width:100%;">
                                            <div class="overlay"></div>
                                            <div class="text"><?php echo $resort_video_title; ?></div>
                                        </a>
                                    </div>
                                </div>
                            <?php
                            }

                            if($resort_marketing_thumbnail[0] != '' && $resort_marketing_title != ''){ ?>
                                <div class="content_images_about col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="inner_image col-lg-12 col-md-12 col-sm-12 col-xs-12"">
                                        <?php
                                        $MarketingListingUrl = get_permalink();
                                        $NewMarketingListingUrl = rtrim($MarketingListingUrl,"/").'?type=marketing';
                                        ?>
                                        <a href="<?php echo $NewMarketingListingUrl; ?>"><img src="<?php echo $resort_marketing_thumbnail[0]; ?>" alt="Resort Marketing" class="image" style="width:100%;">
                                            <div class="overlay"></div>
                                            <div class="text"><?php echo $resort_marketing_title; ?></div>
                                        </a>
                                    </div>
                                </div>
                            <?php
                            }

                            if($resort_logo_thumbnail[0] != '' && $resort_logo_title != ''){ ?>
                                 <div class="content_images_about col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="inner_image col-lg-12 col-md-12 col-sm-12 col-xs-12"">
                                        <?php
                                        $LogoListingUrl = get_permalink();
                                        $NewLogoListingUrl = rtrim($LogoListingUrl,"/").'?type=logo';
                                        ?>
                                        <a href="<?php echo $NewLogoListingUrl; ?>"><img src="<?php echo $resort_logo_thumbnail[0]; ?>" alt="Resort Logo" class="image" style="width:100%;">
                                            <div class="overlay"></div>
                                            <div class="text"><?php echo $resort_logo_title; ?></div>
                                        </a>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

    		<?php
            //Get Contact Details
            $contact_email = get_field('contact_email');
            $contact_location = get_field('contact_location');
            $front_desk_number_1 = get_field('front_desk_number_1');
            $front_desk_number_2 = get_field('front_desk_number_2');
            $reservation_desk_number_1 = get_field('reservation_desk_number_1');
            $reservation_desk_number_2 = get_field('reservation_desk_number_2');
            ?>

            <?php
            if(!empty($contact_email) || !empty($contact_location) || !empty($front_desk_number_1) || !empty($front_desk_number_2) || !empty($reservation_desk_number_1) || !empty($reservation_desk_number_2)){ ?>
                <div class="contact_header">
                    <h1>CONTACT US</h1>
                        <div class="contactUsFooter">
                    	<?php
                        if(!empty($contact_email)){ ?>
                            <a class="" role="button" onclick="collapseEmail()" data-toggle="collapse" href="#collapseEmail" aria-expanded="false" aria-controls="collapseEmail">
                            	<img src="<?php echo $theme_path; ?>/assets/img/email_icon.png" class="email_icon">
                            </a>
                        <?php }
                        if(!empty($contact_location)){ ?>
                            <a class="" role="button" onclick="collapseLocation()" data-toggle="collapse" href="#collapseLoc" aria-expanded="false" aria-controls="collapseLoc">
                            	<img src="<?php echo $theme_path; ?>/assets/img/location_icon.png" class="loc_icon">
                            </a>
                        <?php }
                        if(!empty($front_desk_number_1) || !empty($front_desk_number_2) || !empty($reservation_desk_number_1) || !empty($reservation_desk_number_2)){ ?>
                            <a class="" role="button" onclick="collapseCall()" data-toggle="collapse" href="#collapseCall" aria-expanded="false" aria-controls="collapseCall">
                            	<img src="<?php echo $theme_path; ?>/assets/img/tel_icon.png" class="tel_icon">
                            </a>

                        <?php }
                            if(!empty($contact_email)){ ?>
                                <div class="collapse " id="collapseEmail">
                                    <div class="well email">
                                    <button class="close-btn" type="button" data-toggle="collapse" data-target="#collapseEmail" aria-expanded="false" aria-controls="collapseEmail">
                                    <img src="<?php echo $theme_path; ?>/assets/img/close-small.png">
                                    </button>
                                        <p>
                                            <strong><?php echo $resort_short_name[0]; ?></strong>
                                            <br><?php echo $contact_email; ?>
                                    </p>
                                    </div>
                                </div>
                            <?php
                            }

                            if(!empty($contact_location)){ ?>
                                <div class="collapse " id="collapseLoc">
                                    <div class="well loc">
                                    <button class="close-btn" type="button" data-toggle="collapse" data-target="#collapseLoc" aria-expanded="false" aria-controls="collapseLoc">
                                    <img src="<?php echo $theme_path; ?>/assets/img/close-small.png">
                                    </button>
                                        <p>
                                            <strong><?php echo $resort_short_name[0]; ?></strong>
                                            <br><?php echo $contact_location; ?>
                                        </p>
                                    </div>
                                </div>
                            <?php
                            }

                            if(!empty($front_desk_number_1) || !empty($front_desk_number_2) || !empty($reservation_desk_number_1) || !empty($reservation_desk_number_2)){ ?>
                                <div class="collapse " id="collapseCall">
                                    <div class="well tel">
                                    <button class="close-btn" type="button" data-toggle="collapse" data-target="#collapseCall" aria-expanded="false" aria-controls="collapseCall">
                                    <img src="<?php echo $theme_path; ?>/assets/img/close-small.png">
                                    </button>
                                        <p>  <strong><?php echo $resort_short_name[0]; ?></strong>
                                        <?php
                                        if(!empty($front_desk_number_1) || !empty($front_desk_number_2)){
                                            echo '<br>Front Desk<br>
                                                    '.$front_desk_number_1.'<br>
                                                    '.$front_desk_number_2.'<br>';
                                        }
                                        if(!empty($reservation_desk_number_1) || !empty($reservation_desk_number_2)){
                                            echo '<br>Reservations<br>
                                                    '.$reservation_desk_number_1.'<br>
                                                    '.$reservation_desk_number_2.'</p>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>

                        </div>
                </div>
                <?php } ?>
            </div>
    <?php
    endwhile;
}

get_footer(); ?>
<script>
// When the user clicks on div, open the popup
function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}
function collapseEmail(){
    $('#collapseLoc').removeClass('in');
    $('#collapseLoc').attr('aria-expanded',false);
    $('#collapseCall').removeClass('in');
    $('#collapseCall').attr('aria-expanded',false);
}
function collapseLocation(){
    $('#collapseEmail').removeClass('in');
    $('#collapseEmail').attr('aria-expanded',false);
    $('#collapseCall').removeClass('in');
    $('#collapseCall').attr('aria-expanded',false);
}
function collapseCall(){
    $('#collapseEmail').removeClass('in');
    $('#collapseEmail').attr('aria-expanded',false);
    $('#collapseLoc').removeClass('in');
    $('#collapseLoc').attr('aria-expanded',false);
}
$('.container a').click(function(){
    var $target = $($(this).data('target'));
    if(!$target.hasClass('in'))
        $('.container .in').removeClass('in').height(0);
});

</script>
