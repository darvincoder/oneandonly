<?php
/*
Template Name: Download History
*/
get_header();
$theme_path = get_template_directory_uri();

if (is_user_logged_in()) { ?>
	<div class="main_container_basket edit-s">

        <div class="content_container_left col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="header_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="header_row row">
                    <div class="main_header">
                        <div class="sub_header_icon">
                        <h1>DOWNLOAD HISTORY</h1><img src="" width="40" style="margin-top: -15px;">  
                        </div>
                    </div>                                        
                </div>                                    
            </div>

            <div class="left_col col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <div class="content_row_search row">
                    <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content_row_search_inner row">
                            <div class="left_col_detail">
                                <div class="left_col_detail_header">
                                    <h4>DOWNLOAD OPTIONS</h4> 
                                    <h5>Versions available for download:</h5>
                                    <div class="left_col_detail_list">
									    <ul class="full-ul">
									        <li><a href="#" class="selected">Images Only <i class="radiobtn"></i></a></li>
									        <li><a href="#" class="non_selected">Video Only <i class="radiobtn"></i></a></li>
									        <li><a href="#" class="non_selected">Marketing Collateral Only <i class="radiobtn"></i></a></li>
									        <li><a href="#" class="non_selected">Logos & Motifs Only <i class="radiobtn"></i></a></li>
									        <li><a href="#" class="non_selected">All Assets <i class="radiobtn"></i></a></li>
									    </ul>
                                    </div>                           
                                </div>
                        	</div>
                        	<div class="menu_detail_icons">                                                        
	                            <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_delete.png" class="image_detail_icons"></a>
	                            <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_download.png" class="image_detail_icons"></a>
                        	</div>    
                        </div>
                    </div>
                </div>
            </div>        

        	<div class="content_container_right container col-lg-9 col-md-12 col-sm-12 col-xs-12">
            	<div class="row col_row">
                	<div class="sub_header_2">
                        <div class="sub_header_content">
                            IMAGES
                        </div>                       
                        <div class="sub_header_icon">
                            <img src="<?php echo $theme_path; ?>/assets/img/camera_icon.png" width="25" style="margin-top: -10px;">
                        </div>                        
                    </div>                    
            		<div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    	<div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    		<div class="">
                            	<img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
                            	<div class="image_detail_search">
                                	<h4><b>One&Only Ocean Club</b></h4> 
                                	<h5>Crescent Wing Exterior</h5> 
                                	<div class="ref_detail">
                                    	<div class="ref_detail_text">
                                    		<span>Downloaded: 26-05-2017</span><br>
                                    		<span>Maximum Resolution: 5000 x 3000 TIFF</span>
                                    	</div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
                                	</div>
                            	</div>
                        	</div>
                    	</div>
                    	<div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    		<div class="">
                            	<img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
                            	<div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
		                                    <span>Downloaded: 26-05-2017</span><br>
		                                    <span>High Resolution: 9MB JPG</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>                                        
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
                            	</div>
                        	</div>
                    	</div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>Low Resolution: 3.2MB JPG</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                        <div class="header_row row">
	                            <div class="sub_header">
	                                <div class="sub_header_content_2">
	                                    VIDEOS
	                                </div>

	                                <div class="sub_header_icon">
	                                <img src="<?php echo $theme_path; ?>/assets/img/video_icon.png" width="25" style="margin-top: -10px;">
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>MP4 300KB</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                       <!-- <a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>MP4 300KB</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>MP4 300KB</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                       <!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>-->
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                        <div class="header_row row">
	                            <div class="sub_header">
	                                <div class="sub_header_content_2">
	                                    MARKETING COLLATERAL
	                                </div>

	                                <div class="sub_header_icon">
	                                <img src="<?php echo $theme_path; ?>/assets/img/marketing_icon.png" width="25" style="margin-top: -10px;">
	                                </div>
	                            </div>
	                        </div>
	                    </div>
						<div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>PDF 900KB</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>PDF 1.2MB</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>PDF 5MB</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div> 
	                   <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                        <div class="header_row row">
	                            <div class="sub_header">
	                                <div class="sub_header_content_2">
	                                    LOGOS & MOTIFS
	                                </div>

	                                <div class="sub_header_icon">
	                                <img src="<?php echo $theme_path; ?>/assets/img/camera_icon.png" width="25" style="margin-top: -10px;">
	                                </div>
	                            </div>
	                        </div>
	                    </div>                  
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>Maximum Resolution: 5000 x 3000 TIFF</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>Maximum Resolution: 5000 x 3000 TIFF</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <img src="<?php echo $theme_path; ?>/assets/img/home_exterior1.jpg" alt="Avatar" class="image" style="width:100%">
	                            <div class="image_detail_search">
	                                <h4><b>One&Only Ocean Club</b></h4> 
	                                <h5>Crescent Wing Exterior</h5> 
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: 26-05-2017</span><br>
	                                    <span>Maximum Resolution: 5000 x 3000 TIFF</span>
	                                    </div>
	                                    <div class="detail_icons">
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
            		</div>
        		</div>
        	</div>
    	</div>

<?php }else{
      	echo "<center><h2>Sorry! You can not access this page.</h2><h3>You have to login first. <a href=".get_home_url().'/login/'." style='color:#645c59'>Click here</a></h3></center>";
} ?>
    
<?php
get_footer();
?>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}

var acc = document.getElementsByClassName("accordion_2");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script> 

<script>
// When the user clicks on div, open the popup
function myFunction() {
var popup = document.getElementById("myPopup");
popup.classList.toggle("show");
}
</script>