<?php
/*
Template Name: Privacy Policy
*/
get_header();
$theme_path = get_template_directory_uri();
while ( have_posts() ) : the_post();
	$banner_image = get_field('banner_image');
	$banner_image_value = wp_get_attachment_image_src($banner_image['id'], 'resort_details_banner_image');
	?>
	<div class="full-slider">
	    <div class="slider_container">
	        <img src="<?php echo $banner_image_value[0]; ?>" alt="Terms">
	    </div>
	</div>
	<div class="content_container">
		<div class="content_row row">
			<div class="col-xs-12 contant_padding_small text-left">
				<h2><?php the_field('banner_title') ?></h2>
				<?php the_content();?>				
				<div class="terms_footer">
					<a href="<?php echo get_home_url().'/resort-home/'; ?>" class="c_button">RETURN TO HOME</a>
				</div>
			</div>
		</div>
	</div>
	<?php
endwhile;
?>
<?php
get_footer();
?>