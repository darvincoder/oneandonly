<?php
/*
Template Name: Home Page
*/
get_header();
?>
<?php
$theme_path = get_template_directory_uri();
$banner_image_arr = get_field('banner_image');
$banner_image = wp_get_attachment_image_src($banner_image_arr['id'], 'page_banner');
?>
    <div class="slider_container">
        <div class="slider_content_row row">
            <div class="slider_content col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="home_banner_img">
                <img src="<?php echo $banner_image[0]; ?>" alt="" style="width: 100%;" />
                <!--<div class="slider">

                    <img src="img/homehero1.jpg" class="slider_image" alt="" />
                    <img src="img/jazz_fun.jpg" class="slider_image" alt="" />
                    <img src="img/homehero1.jpg" class="slider_image" alt="" />
                    <img src="img/jazz_fun.jpg" class="slider_image" alt="" />
                </div> -->
            </div>
        </div>
    </div>
<?php
$content_logo_arr = get_field('content_logo');
$content_logo = wp_get_attachment_image_src($content_logo_arr['id'], '');
$content_title = get_field('content_title');
$about_title = get_field('home_about_title');
$brand_title = get_field('brand_title');
$brand_image_arr = get_field('brand_image');
$brand_image = wp_get_attachment_image_src($brand_image_arr['id'], 'home_content_brand');
$resort_title = get_field('resort_title');
?>
    <div class="content_container">
        <div class="content_row row">
            <div class="content_header_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="content_header_image">
                    <img src="<?php echo $content_logo[0]; ?>" width="40">
                </div>
                <div class="home_welcome_title"><h1><?php echo $content_title; ?></h1></div>
                <!-- <div class="home_welcome_about"><h3><?php echo $about_title; ?></h3></div> -->

                <?php
                    if ( have_posts() ) :
                     while ( have_posts() ) : the_post();
                        the_content();
                     endwhile;
                    endif;
                ?>

                <div class="home_welcome_title"><h3><?php echo $brand_title; ?></h3></div>
                <!-- <h3><?php echo $brand_title; ?></h3> -->

                 <div class="content_images_3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner_image col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <a href="#"><img src="<?php echo $brand_image[0]; ?>" alt="Avatar" class="image" style="width:100%">
                            <div class="overlay"></div>

                            <div class="text">
                                <a href="<?php echo get_permalink('573'); ?>"><div class="banner_text">ONE&ONLY</div></a>
                            </div>

                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="partner">
            <div class="home_welcome_title"><h3><?php echo $resort_title; ?></h3></div>
            <!-- <h3><?php echo $resort_title; ?></h3> -->
        </div>

        <?php get_template_part( 'template-parts/page/content', 'front-resort-list' ); ?>

    </div>
<?php
get_footer();
?>
