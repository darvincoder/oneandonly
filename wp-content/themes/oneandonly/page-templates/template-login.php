<?php
/*
Template Name: Login Page
*/
get_header();
$theme_path = get_template_directory_uri();

$logo_image_id = get_field('logo','option');
$logo_image = wp_get_attachment_image_src($logo_image_id, '');
while ( have_posts() ) : the_post();
	$banner_image = get_field('banner_image');
	$banner_image_value = wp_get_attachment_image_src($banner_image['id'], 'resort_details_banner_image');
?>
		<div class="header_container">
		    <div class="nav">
		        <nav class="navbar navbar-inverse">
		            <div class="container-fluid">
		                <div class="navbar-header">
		                    <div class="nav_logo">
		                        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo $logo_image[0]; ?>" width="100%"></a>
		                    </div>
		                </div>
		                <div class="top-hedar-btn">
		                    <a href="<?php echo get_home_url().'/registration/' ?>" class="cbtn">REGISTER</a>
		                </div>

		                <div>
		                </div>
		            </div>
		        </nav>
		    </div>
		</div>
        <div class="banner_container">
            <div class="banner_content_row row">
                <div class="banner_content_account col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<img src="<?php echo $banner_image_value[0]; ?>" alt="Login">
                    <div>
                        <div class="cover_content" id="login_form">
                            <span class="cover_header"></span>
                            <div class="alert alert-danger" id="error_message" style="display: none;"></div>
                                <div class="form_header">
                                     <h3>ENTER YOU DETAILS TO LOG IN</h3>
                                </div>
                                <input type="hidden" name="login_home_url" id="login_home_url" value="<?php echo get_home_url().'/resort-home/' ?>">
                                <div class="single-input-center">
                                    <div class="text_fields">
                                        <input type="text" placeholder="Username or Email Address *" tabindex=1 name="login_username" id="login_username" required>
                                        <input type="password" placeholder="Password *" tabindex=2 name="login_password" id="login_password" required>
                                    </div>
                                </div>
                                <div class="form-bottom-btn">
                                    <div class="forgot">
                                        <span class="psw">Forgotten your password?  <a href="javascript:void(0)" onclick="form_forgetpass()"><strong>Click here</strong></a> to retrieve your password</span>
                                    </div>
                                    <div class="login">
                                        <a href="javascript:void(0)" id="newSignin" name="newSignin" onclick="newSignin()" tabindex=3 class="c_button"> Sign In</a>
                                    </div>
                                    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                                </div>
                        </div>
                        <div class="cover_content" id="forget_password_form" style="display: none;">
                            <span class="cover_header"></span>
                            <div class="alert alert-danger" id="error_message" style="display: none;"></div>
                                <div class="form_header">
                                     <h3>ENTER YOUR EMAIL</h3>
                                </div>
                                <div class="single-input-center">
                                    <div class="text_fields">
                                        <input type="text" placeholder="Enter Email Address *" tabindex=1 name="forget_email" id="forget_email" required>
                                    </div>
                                </div>
                                <div class="form-bottom-btn">
                                    <div class="login">
                                        <a href="javascript:void(0)" id="newForgetPassword" name="newForgetPassword" onclick="newForgetPassword()" tabindex=2 class="c_button"> Submit</a>
                                        <a href="javascript:void(0)" id="closeForgetPassword" name="closeForgetPassword" onclick="closeForgetPassword()" tabindex=2 class="c_button"> Close</a>
                                    </div>
                                </div>
                        </div>
                        <div class="cover_content" id="update_thankyou" style="display: none;">
                            <div class="form">
                                <span class="cover_header"></span>
                                <div class="form_header">
                                    <h3>THANK YOU</h3>
                                    <p>New password has been sent to your registered account.</p>
                                </div>
                                <div class="form-bottom-btn" id="redirect_home_btn">
                                     <a href="<?php echo get_home_url().'/home/' ?>" class="c_button">HOME</a>
                                     <a href="<?php echo get_home_url().'/sign-in/' ?>" class="c_button">SIGN IN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
		endwhile;
?>
<?php
get_footer();
?>
<script type="text/javascript">
    function form_forgetpass() {
        $("#login_form").hide();
        $("#forget_password_form").show();
    }
    function closeForgetPassword(){
        $("#login_form").show();
        $("#forget_password_form").hide();
    }
</script>
