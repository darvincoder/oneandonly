<?php
/*
Template Name: Welcome Page
*/
get_header();
?>
<?php 
$bg_image = get_field('background_image');
$welcome_logo_arr = get_field('welcome_logo');
$welcome_logo = wp_get_attachment_image_src($welcome_logo_arr['id'], 'welcome_logo');
?>
<section class="welcome_body" style="background-image: url(<?php echo $bg_image['url']; ?>);">
    <div class="welcome_section ">
        <div class="container">
            <div class="display-table">
                <div class="disaplay-ce-ll-center">
                    <div class="cover_image">
                        <div class="text-center">
                            <span class="cover_header">
                                <?php
                                if(!empty($welcome_logo[0])){
                                    echo '<img src="'.$welcome_logo[0].'" alt="One & Only ">';
                                }
                                ?>                                
                            </span>
                            <br>
                            <div class="cover_header_2">
                                <?php 
									if ( have_posts() ) :			
										while ( have_posts() ) : the_post();
											the_content(); 
										endwhile;
									endif; 
								?>								
		                        <br><br>
		                        <h4 class="bigfonth4">EXPLORE OUR BRAND ASSETS</h4>
		                        <br>
                            </div>    
                            <div class="enter_button">
                                <a href="<?php echo  get_field('button_url'); ?>"><?php echo get_field('button_text'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </div>
</section>

<?php
get_footer();
?>

