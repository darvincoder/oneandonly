<?php
/*
Template Name: Download History
*/
get_header();
$theme_path = get_template_directory_uri();
$theme_path = get_template_directory_uri();
$CurrentUrl = get_home_url();
$allimages = [];
$allvideos = [];
$allmarketing = [];
$alllogos = [];
$allassets = [];
// $NewImageUrl = $CurrentUrl.'/resort/royal-mirage-dubai?type=image&slug=image_name';
// $NewVideoUrl = $CurrentUrl.'/resort/royal-mirage-dubai?type=video&slug=video_name';
// $NewMarketingUrl = $CurrentUrl.'/resort/royal-mirage-dubai?type=marketing&slug=marketing_name';
// $NewLogoUrl = $CurrentUrl.'/resort/royal-mirage-dubai?type=logo&slug=logo_name';
global $wpdb;
?>

    <div class="main_container_basket edit-s">

        <div class="content_container_left col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="header_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="header_row row">
                    <div class="main_header">
                        <div class="sub_header_icon">
                        <h1>DOWNLOAD HISTORY</h1><img src="" width="40" style="margin-top: -15px;">
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="left_col col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <div class="content_row_search row">
                    <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content_row_search_inner row">
                            <div class="left_col_detail">
                                <div class="left_col_detail_header">
                                    <h4>DOWNLOAD OPTIONS</h4>
                                    <h5>Versions available for download:</h5>
                                    <div class="left_col_detail_list">
									    <ul class="full-ul">
									        <ul class="full-ul">
        <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="all-download">
          <input type="hidden" name="action" value="bulk_download_assets">
        <li>
          <a href="#" class="selected">Images Only <input type="checkbox" class="chk" required>
          </a>
          <input disabled type="hidden" name="allimages" value="<?php echo json_encode($allimages); ?>">
        </li>
        <li>
          <a href="#" class="non_selected">Video Only <input type="checkbox" class="chk" required>
          </a>
          <input disabled type="hidden" name="allvideos" value="<?php echo json_encode($allvideos); ?>">
        </li>
        <li>
          <a href="#" class="non_selected">Marketing Collateral Only <input type="checkbox" class="chk" required>
          </a>
          <input disabled type="hidden" name="allmarketing" value="<?php echo json_encode($allmarketing); ?>">
        </li>
        <li>
          <a href="#" class="non_selected">Logos & Motifs Only <input type="checkbox" class="chk" required>
          </a>
          <input disabled type="hidden" name="alllogos" value="<?php echo json_encode($alllogos); ?>">
        </li>
        <li>
          <a href="#" class="non_selected">All Assets <input type="checkbox" class="chk" required>
          </a>
          <input disabled type="hidden" name="allassets" value="<?php echo json_encode($allassets); ?>">
        </li>
      </form>
    </ul>
									    </ul>
                                    </div>
                                </div>
                        	</div>
                        	<div class="menu_detail_icons">
	                            <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_delete.png" class="image_detail_icons"></a>
	                            <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_download.png" class="image_detail_icons"></a>
                        	</div>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="left_col col-lg-3 col-md-12 col-sm-12 col-xs-12">
              <div class="content_row_search row">
                <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="content_row_search_inner row">
                    <div class="left_col_detail">
                      <div class="left_col_detail_header">
                        <h4>DOWNLOAD OPTIONS</h4>
                        <h5>Versions available for download:</h5>
                        <div class="left_col_detail_list">
                          <ul class="full-ul">
                            <ul class="full-ul">
                              <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="all-download" method="POST">
                                <input type="hidden" name="action" value="bulk_download_assets">
                                <li>
                                  <a href="#" class="selected">Images Only <input type="checkbox" class="chk" name="alldownload">
                                  </a>
                                  <?php //dd(json_encode($allimages)) ?>
                                  <input disabled type="hidden" name="allimages" value='<?php echo json_encode($allimages); ?>'>
                                </li>
                                <li>
                                  <a href="#" class="non_selected">Video Only <input type="checkbox" class="chk" name="alldownload">
                                  </a>
                                  <input disabled type="hidden" name="allvideos" value='<?php echo json_encode($allvideos); ?>'>
                                </li>
                                <li>
                                  <a href="#" class="non_selected">Marketing Collateral Only <input type="checkbox" class="chk" name="alldownload">
                                  </a>
                                  <input disabled type="hidden" name="allmarketing" value='<?php echo json_encode($allmarketing); ?>'>
                                </li>
                                <li>
                                  <a href="#" class="non_selected">Logos & Motifs Only <input type="checkbox" class="chk" name="alldownload">
                                  </a>
                                  <input disabled type="hidden" name="alllogos" value='<?php echo json_encode($alllogos); ?>'>
                                </li>
                                <li>
                                  <a href="#" class="non_selected">All Assets <input type="checkbox" class="chk" name="alldownload">
                                  </a>
                                  <input disabled type="hidden" name="allassets" value='<?php echo json_encode($allassets); ?>'>
                                </li>
                              </ul>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="menu_detail_icons">
                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/right_col_delete.png" class="image_detail_icons"></a>
                        <?php if(is_user_logged_in()) : ?>
                          <button type="submit" class="download-btn">
                            <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                          </button>
                        <?php else: ?>
                          <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                            <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                          </a>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>

        	<div class="content_container_right container col-lg-9 col-md-12 col-sm-12 col-xs-12">
            	<div class="row col_row">
                	<div class="sub_header_2">
                        <div class="sub_header_content">
                            IMAGES
                        </div>
                        <div class="sub_header_icon">
                            <img src="<?php echo $theme_path; ?>/assets/img/camera_icon.png" width="25" style="margin-top: -10px;">
                        </div>
                    </div>
            		<div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <?php
                        $images = $wpdb->get_results("SELECT * FROM wp_download_history WHERE user_id=".get_current_user_id()." AND type='resort_images'");
                      ?>
                      <?php foreach ($images as $image) { ?>
                        <?php $imageacfs = get_field('add_image_data',$image->post_id); ?>
                        <?php foreach($imageacfs as $imageacf) { ?>
                          <?php if($imageacf['image_keywords'] == $image->keyword) { ?>
                    	<div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    		<div class="">
                            	<a href="<?php echo get_permalink($image->post_id).'?slug='.$imageacf['image_keywords']; ?>"><img src="<?php echo $imageacf['add_low_resolution_web']['url']; ?>" alt="Avatar" class="image" style="width:100%"></a>
                            	<div class="image_detail_search">
                                	<h4 class="content_search_desc"><b><?php echo get_the_title($image->post_id) ?></b></h4>
                                	<h5 class="content_search_desc"><?php echo $imageacf['image_name'] ?></h5>
                                	<div class="ref_detail">
                                    	<div class="ref_detail_text">
                                    		<span>Downloaded: <?php echo $image->date ?></span><br>
                                    		<span>Maximum Resolution: 5000 x 3000 TIFF</span>
                                    	</div>
	                                    <div class="detail_icons img_search">

                                        <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>


                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                                        <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="image-download">
                                          <input type="hidden" name="action" value="single_resort_download_assets">
                                          <input type="hidden" name="postid" value="<?php echo $image->post_id ?>">
                                          <input type="hidden" name="keyword" value="<?php echo $imageacf['image_keywords'] ?>">
                                          <input type="hidden" name="type" value="resort_images">
                                          <?php
                                          foreach (json_decode($image->files) as $imagedfileUrl) {
                                              echo '<input type="hidden" name="images[]" value="'.$imagedfileUrl.'">';
                                              array_push($allimages,$imagedfileUrl);
                                              array_push($allassets,$imagedfileUrl);
                                            }
                                            ?>
                                            <?php if(is_user_logged_in()) : ?>
                                            <button type="submit" class="download-btn cart-detail">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($image->files)); ?></span>
                                            </button>
                                          <?php else: ?>
                                            <a class="cart-detail" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($image->files)); ?></span>
                                            </a>
                                          <?php endif; ?>
                                        </form>
                                        <!-- Button trigger modal -->



	                                        <!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a> -->
	                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
	                                        <!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a> -->
	                                    </div>
                                	</div>
                            	</div>
                        	</div>
                    	</div>
                    <?php }}} ?>

	                    <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                        <div class="header_row row">
	                            <div class="sub_header">
	                                <div class="sub_header_content_2">
	                                    VIDEOS
	                                </div>

	                                <div class="sub_header_icon">
	                                <img src="<?php echo $theme_path; ?>/assets/img/video_icon.png" width="25" style="margin-top: -10px;">
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                      <?php
                        $videos = $wpdb->get_results("SELECT * FROM wp_download_history WHERE user_id=".get_current_user_id()." AND type='video'");
                       ?>
                       <?php foreach ($videos as $video) { ?>
                         <?php $videoacfs = get_field('add_video_data',$video->post_id); ?>
                         <?php //dd($videoacfs) ?>
                         <?php foreach($videoacfs as $videoacf) { ?>
                           <?php if($videoacf['video_keyword'] == $video->keyword) { ?>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <a href="<?php echo get_permalink($video->post_id).'?type=video&slug='.$videoacf['video_keyword']; ?>">
                                <video width="100%" height="auto" controls controlsList="nodownload">
                                <source src="<?php echo $videoacf['add_low_resolution_video_360p']['url']; ?>" type="video/mp4">
                                  Your browser does not support HTML5 video.
                                </video>
                              </a>
	                            <div class="image_detail_search">
	                                <h4 class="content_search_desc"><b><?php echo get_the_title($video->post_id) ?></b></h4>
	                                <h5 class="content_search_desc"><?php echo $videoacf['video_name'] ?></h5>
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: <?php echo $video->date ?></span><br>
	                                    <span>MP4 300KB</span>
	                                    </div>
	                                    <div class="detail_icons">
                                        <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                                        <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="image-download">
                                          <input type="hidden" name="action" value="single_resort_download_assets">
                                          <input type="hidden" name="postid" value="<?php echo $video->post_id ?>">
                                          <input type="hidden" name="keyword" value="<?php echo $videoacf['video_keyword'] ?>">
                                          <input type="hidden" name="type" value="video">
                                          <?php
                                          foreach (json_decode($video->files) as $video) {
                                              echo '<input type="hidden" name="videos[]" value="'.$video.'">';
                                              array_push($allvideos,$video);
                                              array_push($allassets,$video);
                                            }
                                            ?>
                                            <?php if(is_user_logged_in()) : ?>
                                            <button type="submit" class="download-btn cart-detail">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($video->files)); ?></span>
                                            </button>
                                          <?php else: ?>
                                            <a class="cart-detail" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($video->files)); ?></span>
                                            </a>
                                          <?php endif; ?>
                                        </form>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    <?php }}} ?>

	                    <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                        <div class="header_row row">
	                            <div class="sub_header">
	                                <div class="sub_header_content_2">
	                                    MARKETING COLLATERAL
	                                </div>

	                                <div class="sub_header_icon">
	                                <img src="<?php echo $theme_path; ?>/assets/img/marketing_icon.png" width="25" style="margin-top: -10px;">
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                      <?php
                      $marketingcollaterals = $wpdb->get_results("SELECT * FROM wp_download_history WHERE user_id=".get_current_user_id()." AND type='marketing'");
                      foreach ($marketingcollaterals as $marketingcollateral) {
                        $marketingcollateralacfs = get_field('add_marketing_collateral',$marketingcollateral->post_id);
                        foreach($marketingcollateralacfs as $marketingcollateralacf) {
                          if($marketingcollateralacf['marketing_collateral_keywords'] == $marketingcollateral->keyword) {
                       ?>
						          <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <a href="<?php echo get_permalink($marketingcollateral->post_id).'?type=marketing&slug='.$marketingcollateralacf['marketing_collateral_keywords']; ?>"><img src="<?php echo $marketingcollateralacf['add_marketing_collateral_file']['url']; ?>" alt="Avatar" class="image" style="width:100%"></a>
	                            <div class="image_detail_search">
	                                <h4 class="content_search_desc"><b><?php echo get_the_title($marketingcollateral->post_id) ?></b></h4>
	                                <h5 class="content_search_desc"><?php echo $marketingcollateralacf['image_name'] ?></h5>
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: <?php echo $marketingcollateral->date ?></span><br>
	                                    <span>PDF 900KB</span>
	                                    </div>
	                                    <div class="detail_icons">
                                        <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                                        <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="image-download">
                                          <input type="hidden" name="action" value="single_resort_download_assets">
                                          <input type="hidden" name="postid" value="<?php echo $marketingcollateral->post_id ?>">
                                          <input type="hidden" name="keyword" value="<?php echo $marketingcollateralacf['marketing_collateral_keywords'] ?>">
                                          <input type="hidden" name="type" value="marketing">
                                          <?php
                                          foreach (json_decode($marketingcollateral->files) as $marketing) {
                                              echo '<input type="hidden" name="marketing[]" value="'.$marketing.'">';
                                              array_push($allmarketing,$marketing);
                                              array_push($allassets,$marketing);
                                            }
                                            ?>
                                            <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
                                            <?php if(is_user_logged_in()) : ?>
                                            <button type="submit" class="download-btn cart-detail">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($marketingcollateral->files)); ?></span>
                                            </button>
                                          <?php else: ?>
                                            <a class="cart-detail" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($marketingcollateral->files)); ?></span>
                                            </a>
                                          <?php endif; ?>
                                        </form>
	                                        <!-- <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
	                                        <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/download_icon.png" class="image_detail_icons"></a> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    <?php }}} ?>
	                   <div class="header_content_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                        <div class="header_row row">
	                            <div class="sub_header">
	                                <div class="sub_header_content_2">
	                                    LOGOS & MOTIFS
	                                </div>

	                                <div class="sub_header_icon">
	                                <img src="<?php echo $theme_path; ?>/assets/img/camera_icon.png" width="25" style="margin-top: -10px;">
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                      <?php
                        $logos = $wpdb->get_results("SELECT * FROM wp_download_history WHERE user_id=".get_current_user_id()." AND type='logo'");
                        foreach ($logos as $logo) {
                          $logoacfs = get_field('add_logo_data',$logo->post_id);
                          foreach($logoacfs as $logoacf) {
                            if($logoacf['logo_keywords'] == $logo->keyword) {
                       ?>
	                    <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                        <div class="">
	                            <a href="<?php echo get_permalink($logo->post_id).'?type=logo&slug='.$logoacf['logo_keywords']; ?>"><img src="<?php echo $logoacf['add_low_resolution_logo_print_72kb']['url']; ?>" alt="Avatar" class="image" style="width:100%"></a>
	                            <div class="image_detail_search">
	                                <h4 class="content_search_desc"><b><?php echo get_the_title($logo->post_id) ?></b></h4>
	                                <h5 class="content_search_desc"><?php echo $logoacf['logo_name'] ?></h5>
	                                <div class="ref_detail">
	                                    <div class="ref_detail_text">
	                                    <span>Downloaded: <?php echo $logo->date ?></span><br>
	                                    <span>Maximum Resolution: 5000 x 3000 TIFF</span>
	                                    </div>
	                                    <div class="detail_icons">
                                        <a href="#" style="float: left"><img src="<?php echo $theme_path; ?>/assets/img/delete_icon.png" class="image_detail_icons"></a>
                                        <!--<a href="#"><img src="img/link_icon.png" class="image_detail_icons"></a>-->
                                        <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="image-download">
                                          <input type="hidden" name="action" value="single_resort_download_assets">
                                          <input type="hidden" name="postid" value="<?php echo $logo->post_id ?>">
                                          <input type="hidden" name="keyword" value="<?php echo $logoacf['logo_keywords'] ?>">
                                          <input type="hidden" name="type" value="logo">
                                          <?php
                                          foreach (json_decode($logo->files) as $logo) {
                                              echo '<input type="hidden" name="logos[]" value="'.$logo.'">';
                                              array_push($alllogos,$logo);
                                              array_push($allassets,$logo);
                                            }
                                            ?>
                                            <a href="#"><img src="<?php echo $theme_path; ?>/assets/img/link_icon.png" class="image_detail_icons"></a>
                                            <?php if(is_user_logged_in()) : ?>
                                            <button type="submit" class="download-btn cart-detail">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($logo->files)); ?></span>
                                            </button>
                                          <?php else: ?>
                                            <a class="cart-detail" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                                              <span><?php echo count(json_decode($logo->files)); ?></span>
                                            </a>
                                          <?php endif; ?>
                                        </form>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    <?php }}} ?>
            		</div>
        		</div>
        	</div>

    	</div>
<?php
get_footer();
?>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}

var acc = document.getElementsByClassName("accordion_2");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
// When the user clicks on div, open the popup
function myFunction() {
var popup = document.getElementById("myPopup");
popup.classList.toggle("show");
}
$(document).ready(function() {
  $('.chk').click(function() {
    $('.chk').each(function() {
      console.log($(this).parent().parent());
      if($(this).prop("checked")){
        $(this).parent().parent().children('input').prop('disabled', false);
      }else{
        $(this).parent().parent().children('input').prop('disabled', true);
      }
    });
  });
  $("#all-download").validate();
});
</script>
