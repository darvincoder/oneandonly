<?php
/*
Template Name: My Account
*/
get_header();


// while ( have_posts() ) : the_post();
// 	$banner_image = get_field('banner_image');
// 	$banner_image_value = wp_get_attachment_image_src($banner_image['id'], 'resort_details_banner_image');


$theme_path = get_template_directory_uri();
$current_user = wp_get_current_user();
if ( is_user_logged_in() ) {
$user_id = get_current_user_id();
?>

<div class="banner_container">
            <div class="banner_content_row row">

                <div class="banner_content_account col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<?php var_dump(get_field('banner_image',131));
										die();
									?>

									<img src="<?php echo $banner_image_value[0]; ?>" alt="My Account">
                    <div>
                        <!-- <a id="signin_url" style="display: none;" href="<?php// echo wp_logout_url(get_home_url().'/home/'); ?>"></a> -->
                        <input type="hidden" name="signin_url" id="signin_url" value="<?php echo get_home_url().'/wp-login.php?action=logout&_wpnonce='. wp_create_nonce( 'log-out' ).'&redirect_to='.get_home_url().'/resort-home/' ?>">
                        <div class="cover_content" id="update_account_form">
                            <span class="cover_header"></span>
                                <div class="form_header">
                                     <h3>MY ACCOUNT</h3>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="alert alert-danger" id="error_message" style="display: none;"></div>
                                        <div class="col-xs-12">
                                            <div class="col-sm-6">
                                                <div class="text_fields">
                                                	<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                                                    <input type="text" placeholder="First Name"  name="updateFirstname" id="updateFirstname" value="<?php echo $current_user->user_firstname; ?>" required>
                                                    <input type="text" placeholder="Username" readonly="true" name="updateUsername" id="updateUsername" value="<?php echo $current_user->user_login; ?>" required>
                                                    <input type="Password" placeholder="New Password" name="updatePassword" id="updatePassword" value="">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="text_fields">
                                                    <input type="text" placeholder="Surname" name="updateLastname" id="updateLastname" value="<?php echo $current_user->user_lastname; ?>" required>
                                                    <input type="text" placeholder="Email" name="updateEmail" id="updateEmail" value="<?php echo $current_user->user_email; ?>" required>
                                                    <input type="Password" placeholder="Confirm New Password" name="updateConfirmpassword" id="updateConfirmpassword" value="">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 text-center">
                                                <div class="Update">
                                                    <button type="submit" onclick="updateAccount()" class="c_button">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="cover_content" id="update_thankyou" style="display: none;">
                            <div class="form">
                                <span class="cover_header"></span>
                                <div class="form_header">
                                    <h3>THANK YOU</h3>
                                    <p>Your account details has been updated</p>
                                </div>
                                <div class="c_button" id="redirect_mesg" style="display: none;">Please wait while redirecting...</div>

                                <div class="form-bottom-btn" id="redirect_home_btn" style="display: none;">
                                     <a href="<?php echo get_home_url().'/resort-home/' ?>" class="c_button">RETURN TO HOME</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php }else{
	echo "<h2>Soory! You can not access this page.</h2>";
} ?>



<?php
get_footer();
?>
