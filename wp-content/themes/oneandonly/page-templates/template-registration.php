<?php
/*
Template Name: Registration Page
*/
get_header();
$theme_path = get_template_directory_uri();
$logo_image_id = get_field('logo','option');
$logo_image = wp_get_attachment_image_src($logo_image_id, '');
while ( have_posts() ) : the_post();
	$banner_image = get_field('banner_image');
	$banner_image_value = wp_get_attachment_image_src($banner_image['id'], 'resort_details_banner_image');
?>
		<div class="header_container">
            <div class="nav">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <div class="nav_logo">
                                <a href="<?php echo get_home_url().'/resort-home/' ?>"><img src="<?php echo $logo_image[0]; ?>" width="100%"></a>
                            </div>
                        </div>
                        <div>
                            <div class="top-hedar-btn">
                                <a href="<?php echo get_home_url().'/login/' ?>" class="cbtn">SIGN IN</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <div class="banner_container">
            <div class="banner_content_row row">
                <div class="banner_content_account col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<img src="<?php echo $banner_image_value[0]; ?>" alt="Registration">
                    <div>
                        <div class="cover_content">

                            <span class="cover_header">

                            </span>

                            <div class="reg_landing" id="reg_landing">
                                <div class="form_header">
                                     <h3>WELCOME</h3>
                                     <p style="font-size: 20px;">To register, please select a category</p>
                                </div>


                                <div class="single-input-center">
                                    <div class="box">
                                        <a href="#" onclick="get_reg_form('Corporate');" class="c_button" style="text-transform: none;">One&Only Team / Kerzner Team
                                        	<i class="fa fa-chevron-right" style="float: right;margin-top: 3px;" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" onclick="get_reg_form('Travel Professional');"class="c_button" style="text-transform: none;">Travel Professional
                                        	<i class="fa fa-chevron-right" style="float: right;margin-top: 3px;" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" onclick="get_reg_form('Media / Press');"class="c_button" style="text-transform: none;">Media / Press
                                        	<i class="fa fa-chevron-right" style="float: right;margin-top: 3px;" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" onclick="get_reg_form('Other');"class="c_button" style="text-transform: none;">Other
                                        	<i class="fa fa-chevron-right" style="float: right;margin-top: 3px;" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="reg_form" id="reg_form" style="display: none;">
                            	<div class="form_header">
                                     <h3>CREATE A NEW ACCOUNT</h3>
                                     <p style="line-height: 25px; font-size: 20px;">Please create an account to access and download One&Only brand assets. Already have one? <a href="<?php echo get_home_url().'/login/' ?>"><strong style="color: #fff;">Sign in here</strong></a></p>

                                </div>
                               	<div class="row">

	                                <div class="col-xs-12">
                                        <div class="alert alert-danger" id="error_message" style="display: none;"></div>
	                                    <div class="col-xs-12">
	                                    	<input type="hidden" name="reg_type" id="reg_type" value="">
			                                <div class="col-sm-6">
			                                    <div class="text_fields">
			                                        <input type="text" placeholder="First Name *" tabindex=1 name="regFirstname" id="regFirstname" required>
			                                        <input type="text" placeholder="Username *" tabindex=3 name="regUsername" id="regUsername" required>
			                                        <input type="password" placeholder="Password *" tabindex=5 name="regPassword" id="regPassword" required>
			                                    </div>
			                                </div>
											<div class="col-sm-6">
			                                    <div class="text_fields">
			                                        <input type="text" placeholder="Surname *" tabindex=2 name="regLastname" id="regLastname" required>
			                                        <input type="text" placeholder="Email *" tabindex=4 name="regEmail" id="regEmail" required>
			                                        <input type="password" placeholder="Confirm Password *" tabindex=6 name="regConfirmpassword" id="regConfirmpassword" required>
			                                    </div>
		                                	</div>
											<div class="col-sm-12 text-center">
												<input style="margin-top: 20px;" name="term_check" id="term_check" value="0" tabindex=7 type="checkbox"> <span style="font-size: 14px;">By clicking the box you accept our Terms & Conditions. <a href=""  class="link"><b>Click here</b> </a>to view them.</span>
	                                    		<div class="Update_btm">
	                                        		<a href="javascrip:void(0)" id="newRegsitration" onclick="newRegsitration()" tabindex=8 name="newRegsitration" class="c_button" style="padding: 8px 30px;"> Create an account</a>
	                                    		</div>
	                                		</div>
	                                	</div>
									</div>
    							</div>
                            </div>
                            <div class="thankyou_message" id="thankyou_message"  style="display: none;">
                            	<div class="form_header">
                                     <h3>THANK YOU</h3>
                                     <p>Your account has been successfully created.</p>
                                </div>

                                <div class="form-bottom-btn">
                                     <a href="<?php echo get_home_url().'/resort-home/' ?>" class="c_button">RETURN TO HOME</a>
                                     <a href="<?php echo get_home_url().'/login/' ?>" class="c_button">SIGN IN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	endwhile;
?>
<?php
get_footer();
?>
