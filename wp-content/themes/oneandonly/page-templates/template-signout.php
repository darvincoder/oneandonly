<?php
/*
Template Name: Signout Page
*/
get_header();
$theme_path = get_template_directory_uri();
?>

<div class="banner_container">
    <div class="banner_content_row row">
        <div class="banner_content_account col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div>
                <div class="cover_content" id="update_thankyou">
                    <div class="form">
                        <span class="cover_header"></span>                              
                        <div class="form_header">
                            <h3>THANK YOU</h3>
                            <p>You have successfully signed out.</p>
                        </div>                                
                        <div class="form-bottom-btn" id="redirect_home_btn">
                             <a href="<?php echo get_home_url().'/resort-home/'; ?>" class="c_button">RETURN TO HOME</a>                                     
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
   
<?php
get_footer();
?>