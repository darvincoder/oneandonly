<?php
//Today Date
date_default_timezone_set('Europe/London');
$sTime = date("d-m-Y");

global $post;
$theme_path = get_template_directory_uri();

$resort_type = '';
$CurrentUrl = get_permalink();
$image_display = $video_display = $marketing_display = $logo_display = "style='display:none';";
$resort_type = $_REQUEST['type'];
$resort_value = $_REQUEST['slug'];

if(isset($resort_value) && $resort_value != '' && $resort_type != ''){
  if($resort_type == "image"){
    $resort_type_value = "IMAGE";
    $image_display = "style='display:block';";
    $repiter_field = "add_image_data";
    $keyword_field = "image_keywords";
    $name_field    = "image_name";
    $description_field = "image_description";
  }elseif($resort_type == "video"){
    $resort_type_value = "VIDEO";
    $video_display = "style='display:block';";
    $repiter_field = "add_video_data";
    $keyword_field = "video_keyword";
    $name_field    = "video_name";
    $description_field = "video_description";
  }elseif($resort_type == "marketing"){
    $resort_type_value = "MARKETING COLLATERAL";
    $marketing_display = "style='display:block';";
    $repiter_field = "add_marketing_collateral";
    $keyword_field = "marketing_collateral_keywords";
    $name_field    = "marketing_collateral_name";
    $marketing_resort_file = "add_marketing_collateral_file";
    $description_field = "marketing_collateral_description";
  }elseif($resort_type == "logo"){
    $resort_type_value = "LOGOS & MOTIFS";
    $logo_display = "style='display:block';";
    $repiter_field = "add_logo_data";
    $keyword_field = "logo_keywords";
    $name_field    = "logo_name";
    $description_field = "logo_description";
  }
}

$resort_slug = $post->post_name;
$args = array(
  'name'        => $resort_slug,
  'post_type'   => 'resort',
  'post_status' => 'publish',
  'numberposts' => 1
);
$resort_posts = get_posts($args);

$resort_ID = $resort_posts[0]->ID;
$resort = explode(",", $resort_posts[0]->post_title);

$resort_title_location = $resort[0].', '.$resort[1];
$resort_title = $resort[0];

$reosrt_slug = $resort_posts[0]->post_name;

?>

  <?php
    if( have_rows($repiter_field) ):
        while ( have_rows($repiter_field) ) : the_row();
          if(get_sub_field($keyword_field) == $_GET['slug']) :
            ?>

    <div class="content_container_left col-lg-9 col-md-9 col-sm-9 col-xs-12">
    <div class="main_header_title">
        <h1><?php echo strtoupper(get_sub_field($name_field)); ?></h1>
        <h3><?php echo strtoupper(get_the_title()); ?><span>|</span><?php echo $resort_type_value; ?></h3>
        <div class="divider"></div>
    </div>
    <div class="single_video_details" <?php echo $video_display; ?>>
        <div class="left_col_assats col-lg-3 col-md-12 col-sm-12 col-xs-12">
          <div class="content_row_search row">
            <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="content_row_search_inner row">
                <div class="left_col_detail">
                  <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="video-download">
                    <input type="hidden" name="action" value="single_resort_download_assets">
                    <input type="hidden" name="postid" value="<?php echo get_the_ID() ?>">
                    <input type="hidden" name="keyword" value="<?php echo get_sub_field($keyword_field) ?>">
                    <input type="hidden" name="resort_name" value="<?php echo get_sub_field($name_field) ?>">
                    <input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
                    <div class="left_col_detail_header">
                      <h4>DOWNLOAD OPTIONS</h4>
                      <h5>Versions available for download:</h5>
                      <div class="left_col_detail_list">
                        <ul class="full-ul">
                          <?php if (get_sub_field('add_low_resolution_video_360p')): ?>
                            <li><div class="selected">Low Resolution 360P <input type="checkbox" class="chk" value="<?php echo get_sub_field('add_low_resolution_video_360p')['url'] ?>" name="selectVideoRes" ></i></div>
                              <input disabled type="hidden" name="videos[]" value="<?php echo get_sub_field('add_low_resolution_video_360p')['url'] ?>">
                            </li>
                          <?php endif; ?>
                          <?php if (get_sub_field('add_mid_resolution_video_720p')): ?>
                            <li><div class="non_selected">Mid Resolution 720P <input type="checkbox" class="chk" value="<?php echo get_sub_field('add_mid_resolution_video_720p')['url'] ?>" name="selectVideoRes" ></div>
                              <input disabled type="hidden" name="videos[]" value="<?php echo get_sub_field('add_mid_resolution_video_720p')['url'] ?>">
                            </li>
                          <?php endif; ?>
                          <?php if (get_sub_field('add_high_resolution_video_1080p')): ?>
                            <li><div class="non_selected">High Resolution 1080P <input type="checkbox" class="chk" value="<?php echo get_sub_field('add_high_resolution_video_1080p')['url'] ?>" name="selectVideoRes" ></div>
                              <input disabled type="hidden" name="videos[]" value="<?php echo get_sub_field('add_high_resolution_video_1080p')['url'] ?>">
                            </li>
                          <?php endif; ?>
                        </ul>
                      </div>
                      <div class="detail_icon">
                        <a class="add-to-cart" role="button" data-type=<?php echo $resort_type ?> data-postid=<?php echo get_the_ID() ?> data-keyword=<?php echo get_sub_field($keyword_field) ?> data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample">
                          <img src="<?php echo $theme_path.'/assets/img/cart_icon_d.png'; ?>" class="image_detail_icons">
                          <span><?php echo resortPageCount( get_sub_field($keyword_field), get_the_ID(), $resort_type ); ?></span>
                        </a>
                        <?php if (is_user_logged_in()): ?>
                          <button type="submit" class="download-btn">
                            <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                          </button>
                        <?php else: ?>
                          <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                            <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                          </a>
                        <?php endif; ?>
                        <div class="collapse" id="collapseExample">
                          <div class="well">
                            <button class="close-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                              <img src="<?php echo $theme_path.'/assets/img/close-pop.png'; ?>">
                            </button>
                            Assets succesfully added to basket
                          </div>
                        </div>
                        <div class="basket_success_msg">
                          <span>Your Product added to cart</span>
                          <button class="bsk_msg_close"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="basket_error_msg">
                          <span class="error_msg">Please select a video</span>
                        </div>
                      </div>
                      <div class="basket_validate_msg">
                        <span class="error_msg">Please select a video</span>
                      </div>
                    </div>
                  </form>
                  <div class="left_col_detail_list_2">
                    <span>Your basket is currently: 10MB</span><br>
                    <span>The maximum basket size is: 2GB</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="RelatedAssets mb-1">
              <p class="relaetedP"><?php echo strtoupper(get_sub_field($description_field)); ?></p>
          </div>
          <div class="RelatedAssets visible-lg">
                <h3>Related Assets</h3>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets" controlsList="nodownload">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="content_container_righ container col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <div class="row col_row">
                <div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="">
                          <video width="100%" height="auto" controls controlsList="nodownload">
                            <source src="<?php echo get_sub_field('add_low_resolution_video_360p')['url'] ?>" type="video/mp4">
                              Your browser does not support HTML5 video.
                            </video>
                            <div class="">
                                <h4>Usage: Print, Web & TV</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="RelatedAssets visible-xs">
                <h3>Related Assets</h3>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets" controlsList="nodownload">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="#">
                                        <video width="400"  class="videoassets">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.mp4'; ?>" type="video/mp4">
                                            <source src="<?php echo $theme_path.'/assets/video/mov_bbb.ogg'; ?>" type="video/ogg">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
    </div>
    <div class="single_marketing_details" <?php echo $marketing_display; ?>>
        <div class="left_col_assats col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <div class="content_row_search row">
                <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="content_row_search_inner row">
                        <div class="left_col_detail">
                          <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="marketing-download">
                            <input type="hidden" name="action" value="single_resort_download_assets">
                            <input type="hidden" name="postid" value="<?php echo get_the_ID() ?>">
                            <input type="hidden" name="keyword" value="<?php echo get_sub_field($keyword_field) ?>">
                            <input type="hidden" name="resort_name" value="<?php echo get_sub_field($name_field) ?>">
                            <input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
                            <div class="left_col_detail_header">
                                <h4>DOWNLOAD OPTIONS</h4>
                                <h5>Versions available for download:</h5>
                                <div class="left_col_detail_list">
                                    <ul class="full-ul">
                                      <?php if (get_sub_field('add_marketing_collateral_file')): ?>
                                        <li><div class="selected">File (PDF) 1.3MB <input type="checkbox" class="chk" value="<?php echo get_sub_field('add_marketing_collateral_file')['url'] ?>" name="selectMarketingRes" ></div>
                                        <input disabled type="hidden" name="marketing[]" value="<?php echo get_sub_field('add_marketing_collateral_file')['url'] ?>">
                                        </li>
                                      <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="detail_icon">
                                  <a class="add-to-cart" role="button" data-type=<?php echo $resort_type ?>  data-postid=<?php echo get_the_ID() ?> data-keyword=<?php echo get_sub_field($keyword_field) ?> data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample">
                                    <img src="<?php echo $theme_path.'/assets/img/cart_icon_d.png'; ?>" class="image_detail_icons">
                                    <span><?php echo resortPageCount( get_sub_field($keyword_field), get_the_ID(), $resort_type ); ?></span>
                                  </a>
                                  <?php if(is_user_logged_in()) : ?>
                                    <button type="submit" class="download-btn">
                                      <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                                    </button>
                                  <?php else: ?>
                                    <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                                      <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                                    </a>
                                  <?php endif; ?>
                                    <div class="collapse" id="collapseExample">
                                        <div class="well">
                                            <button class="close-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            <img src="<?php echo $theme_path.'/assets/img/close-pop.png'; ?>">
                                            </button>
                                            Assets succesfully added to basket
                                        </div>
                                    </div>
                                    <div class="basket_success_msg">
                                        <span>Your Product added to cart</span>
                                        <button class="bsk_msg_close"><i class="fa fa-times"></i></button>
                                    </div>
                                    <div class="basket_error_msg">
                                      <span class="error_msg">Please select a Marketing Collateral</span>
                                    </div>
                                </div>
                                <div class="basket_validate_msg">
                                  <span class="error_msg">Please select a Marketing Collateral</span>
                                </div>
                            </div>
                          </form>
                            <div class="left_col_detail_list_2">
                                <span>Your basket is currently: 10MB</span><br>
                                <span>The maximum basket size is: 2GB</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="RelatedAssets mb-1">
                <p class="relaetedP"><?php echo strtoupper(get_sub_field($description_field)); ?></p>
            </div>
            <div class="RelatedAssets visible-lg">
                <h3>Related Assets</h3>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-3-m.jpg'; ?>"></a></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-3-m.jpg'; ?>"></a></div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="content_container_right container col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <div class="row col_row">
                <div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="content_images_assats col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="">

                            <?php
                              $resort_data = get_sub_field('add_marketing_collateral_file');
                              $resort_url =  $resort_data['url'];
                              $resort_exe = pathinfo( $resort_url)['extension'];
                              switch ($resort_exe) {
                                case "pdf": ?>
                                    <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pdf.png" alt="Avatar" class="image" style="width:100%"> -->
                                    <embed class="marketing_view image" src="<?php echo $resort_url ?>" alt="Avatar" type="application/pdf">
                                    <?php break;
                                case "jpg":
                                case "jpeg":
                                case "png":
                                case "svg": ?>
                                    <img src="<?php echo get_sub_field('add_marketing_collateral_file')['url']; ?>" alt="Avatar" class="image" style="width:100%">
                                    <?php break;
                                case "eps": ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/eps.png" alt="Avatar" class="image" style="width:100%">
                                <?php break;
                                case "rar":
                                case "zip": ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/zip.png" alt="Avatar" class="image" style="width:100%">
                                    <?php break;
                                default: ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/no_img.jpg" alt="Avatar" class="image" style="width:100%">
                                <?php break;
                              }
                            ?>

                            <h4>Usage: Print & Web.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="RelatedAssets visible-xs">
            <h3>Related Assets</h3>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-3-m.jpg'; ?>"></a></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2-m.jpg'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-3-m.jpg'; ?>"></a></div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
    </div>
    <div class="single_logo_details" <?php echo $logo_display; ?>>
        <div class="left_col_assats col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <div class="content_row_search row">
                <div class="left_col_content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="content_row_search_inner row">
                        <div class="left_col_detail">
                          <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="logo-download">
                            <input type="hidden" name="action" value="single_resort_download_assets">
                            <input type="hidden" name="postid" value="<?php echo get_the_ID() ?>">
                            <input type="hidden" name="keyword" value="<?php echo get_sub_field($keyword_field) ?>">
                            <input type="hidden" name="resort_name" value="<?php echo get_sub_field($name_field) ?>">
                            <input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
                            <div class="left_col_detail_header">
                                <h4>DOWNLOAD OPTIONS</h4>
                                <h5>Versions available for download:</h5>
                                <div class="left_col_detail_list">
                                    <ul class="full-ul">
                                      <?php if(get_sub_field('add_low_resolution_logo_print_72kb')) : ?>
                                        <li><div class="selected">Low Resolution (web) 78KB <input type="checkbox" class="chk" value="<?php echo get_sub_field('add_low_resolution_logo_print_72kb')['url'] ?>" name="selectImageRes" ></i></div>
                                          <input disabled type="hidden" name="logos[]" value="<?php echo get_sub_field('add_low_resolution_logo_print_72kb')['url'] ?>">
                                        </li>
                                      <?php endif; ?>
                                      <?php if (get_sub_field('add_high_resolution_logo_print_370kb')): ?>
                                        <li><div class="non_selected">High Resolution (print) 370KB <input type="checkbox" class="chk" value="<?php echo get_sub_field('add_high_resolution_logo_print_370kb')['url'] ?>" name="selectImageRes" ></div>
                                          <input disabled type="hidden" name="logos[]" value="<?php echo get_sub_field('add_high_resolution_logo_print_370kb')['url'] ?>">
                                        </li>
                                      <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="detail_icon">
                                  <a class="add-to-cart" role="button" data-type=<?php echo $resort_type ?>  data-postid=<?php echo get_the_ID() ?> data-keyword=<?php echo get_sub_field($keyword_field) ?> data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample">
                                    <img src="<?php echo $theme_path.'/assets/img/cart_icon_d.png'; ?>" class="image_detail_icons">
                                    <span><?php echo resortPageCount( get_sub_field($keyword_field), get_the_ID(), $resort_type ); ?></span>
                                  </a>
                                  <?php if(is_user_logged_in()) : ?>
                                  <button type="submit" class="download-btn">
                                    <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                                  </button>
                                  <?php else: ?>
                                    <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                                      <img src="<?php echo $theme_path.'/assets/img/right_col_download.png'; ?>" class="image_detail_icons">
                                    </a>
                                  <?php endif; ?>
                                    <div class="collapse" id="collapseExample">
                                        <div class="well">
                                            <button class="close-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            <img src="<?php echo $theme_path.'/assets/img/close-pop.png'; ?>">
                                            </button>
                                            Assets succesfully added to basket
                                        </div>
                                    </div>
                                    <div class="basket_success_msg">
                                        <span>Your Product added to cart</span>
                                        <button class="bsk_msg_close"><i class="fa fa-times"></i></button>
                                    </div>
                                    <div class="basket_error_msg">
                                      <span class="error_msg">Please select a Logo</span>
                                    </div>
                                </div>
                                <div class="basket_validate_msg">
                                  <span class="error_msg">Please select a Logo</span>
                                </div>
                            </div>
                          </form>
                            <div class="left_col_detail_list_2">
                                <span>Your basket is currently: 10MB</span><br>
                                <span>The maximum basket size is: 2GB</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="RelatedAssets mb-1">
                <p class="relaetedP"><?php echo strtoupper(get_sub_field($description_field)); ?></p>
            </div>
            <div class="RelatedAssets visible-lg">
                <h3>Related Assets</h3>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="content_container_right container col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <div class="row col_row">
                <div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="content_images_assats col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="">
                            <img src="<?php echo get_sub_field('add_low_resolution_logo_print_72kb')['url']; ?>" alt="Avatar" class="image" style="width:100%">
                            <h4>Usage: Print & Web.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="RelatedAssets visible-xs">
                <h3>Related Assets</h3>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-1.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                                <div class="col-xs-4"><a href="#"><img src="<?php echo $theme_path.'/assets/img/asset-2.png'; ?>"></a></div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
    </div>
</div>
<?php endif; ?>
  <?php endwhile; endif; ?>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
  $(document).ready(function() {
    $('.chk').click(function() {
      $('.chk').each(function() {
        if($(this).prop("checked")){
          $(this).parent().parent().children('input').prop('disabled', false);
        }else{
          $(this).parent().parent().children('input').prop('disabled', true);
        }
      });
    });
    $("#image-download").validate();
    $("#video-download").validate();
    $("#marketing-download").validate();
    $("#logo-download").validate();

  });

  var acc = document.getElementsByClassName("accordion");

    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }

    var acc = document.getElementsByClassName("accordion_2");
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }
    function myFunction() {
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }

    function myFunction1() {
        var popup_link = document.getElementById("myPopup1");
        popup_link.classList.toggle("show");
    }

    function myFunction2() {
        var popup_basket = document.getElementById("myPopup2");
        popup_basket.classList.toggle("show");
    }

    $('button.bsk_msg_close').on('click', function () {
        var elements = document.getElementsByClassName('basket_success_msg');
        for(var i=0; i<elements.length; i++) {
          elements[i].style.display='none';
        }
        return false;
    });

    $(document).ready(function(){
        $("form").submit(function(){
        if ($('input:checkbox').filter(':checked').length < 1){
          var validate_msg = document.getElementsByClassName('basket_validate_msg');
          for(var i=0; i<validate_msg.length; i++) {
            validate_msg[i].style.display='block';
            $('.basket_validate_msg').delay(5000).fadeOut();
          }
          return false;
        }
        });
    });

</script>
