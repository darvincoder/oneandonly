<div class="content_row row">
    <div class="content_wrapper_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="content_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="content_row row">
            <?php
            $theme_path = get_template_directory_uri();
            $args = array( 'post_type' => 'resort','post_status' => 'publish', 'posts_per_page' => -1);
                $loop = new WP_Query( $args );
                if($loop->have_posts()):
                    $count = $loop->post_count;
                    while ( $loop->have_posts() ) :
                        $loop->the_post();
                        $is_partner_hotel =  get_field('our_partner_hotel');
                        $our_site_page = get_field('our_site_page');
                        $temp_resort_title = get_the_title();
                        $resort_title_arr = explode(",", $temp_resort_title);
                        $resort_city_arra = explode("One&#038;Only", $resort_title_arr[0]);
                        $company_title = "ONE&ONLY";
                        $company_title_extra = $resort_city_arra[0];
                        $resort_city_title = $resort_city_arra[1];
                        $resort_country_title = $resort_title_arr[1];

                        $resort_thumbnail_image_arr = get_field('resort_thumbnail');
                        // print_r( $resort_thumbnail_image_arr);
                        $resort_thumbnail_image = wp_get_attachment_image_src($resort_thumbnail_image_arr['id'], 'resort_thumbnail');
                        $resort_details_link = get_the_permalink();

                        if(empty($is_partner_hotel) && empty($our_site_page)){
                        ?>
                            <div class="content_images_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="inner_image col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <a href="<?php echo $resort_details_link; ?>">
                                        <img src="<?php echo $resort_thumbnail_image[0]; ?>" alt="Avatar" class="image" style="width:100%;">
                                        <div class="overlay"></div>
                                        <div class="text">
                                            <div><?php echo strtoupper($company_title_extra); ?></div>
                                            <div class="text_header"><?php echo strtoupper($company_title); ?></div>
                                            <div><?php echo strtoupper($resort_city_title); ?></div>
                                            <div><?php echo strtoupper($resort_country_title); ?></div>
                                        </div>

                                    </a>
                                </div>
                            </div>
                        <?php }
                    endwhile;
                endif;
                wp_reset_query();
            ?>
            </div>
        </div>
    </div>
</div>
<div class="content_row row">
    <div class="content_wrapper_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="content_1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="content_row row">
                <?php
                $theme_path = get_template_directory_uri();
                $args = array( 'post_type' => 'resort','post_status' => 'publish', 'posts_per_page' => -1);
                    $loop = new WP_Query( $args );
                    if($loop->have_posts()):
                        $count = $loop->post_count;
                        while ( $loop->have_posts() ) :
                            $loop->the_post();
                            $is_partner_hotel =  get_field('our_partner_hotel');
                            $temp_resort_title = get_the_title();
                            $resort_title_arr = explode(",", $temp_resort_title);
                            $resort_city_arra = explode("One&#038;Only", $resort_title_arr[0]);
                            $company_title = "ONE & ONLY";
                            $company_title_extra = $resort_city_arra[0];
                            $resort_city_title = $resort_city_arra[1];
                            $resort_country_title = $resort_title_arr[1];

                            $resort_thumbnail_image_arr = get_field('resort_thumbnail');
                            // print_r( $resort_thumbnail_image_arr);
                            $resort_thumbnail_image = wp_get_attachment_image_src($resort_thumbnail_image_arr['id'], 'resort_thumbnail');
                            $resort_details_link = get_the_permalink();
                            if(!empty($is_partner_hotel)){
                            ?>
                            <div class="partner">
                                <h3>OUR PARTNER HOTEL</h3>
                            </div>
                                <div class="content_images_2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="inner_image col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <a href="<?php echo $resort_details_link; ?>">
                                            <img src="<?php echo $resort_thumbnail_image[0]; ?>" alt="Avatar" class="image" style="width:100%;">
                                            <div class="overlay"></div>
                                            <div class="text">
                                                <?php echo strtoupper($company_title_extra); ?>
                                                <div class="text_header"><?php echo strtoupper($resort_city_title); ?></div>
                                                <!-- <div><?php echo strtoupper($resort_city_title); ?></div> -->
                                                <div><?php echo strtoupper($resort_country_title); ?></div>
                                            </div>

                                        </a>
                                    </div>
                                </div>
                            <?php }
                        endwhile;
                    endif;
                    wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</div>
