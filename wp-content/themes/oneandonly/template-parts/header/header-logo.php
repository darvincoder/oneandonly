<?php
/**
 * Displays header media
 */

?>

<div class="nav_logo">
    <a href="<?php echo get_site_url(); ?>" title="oneandonly">
    	<?php
		$logo_image_id = get_field('logo','option');
		$logo_image = wp_get_attachment_image_src($logo_image_id, '');
		?>
			<img src="<?php echo $logo_image[0]; ?>" alt="oneandonly" width="100%">
    </a>
</div>

