<?php
/**
 * Displays header media
 */
$theme_path = get_template_directory_uri();
?>
<div class="search_header col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<form>
		<?php
		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
			$login_user_name = $current_user->user_firstname.' '.$current_user->user_lastname;
			?>
			<div class="popup" onclick="myFunction()">
				<img src="<?php echo $theme_path; ?>/assets/img/user.png" width="28" height="28">
	            <span class="popuptext" id="myPopup"><?php echo $login_user_name; ?><hr><a href=<?php echo wp_logout_url(get_home_url().'/signout/'); ?>>Sign Out</a></span>
	        </div>
			<?php
		}
		?>
		<div class="dropdown">
    		<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select a Resort
    			<span class="caret"></span></button>
					<ul class="dropdown-menu header_search">
						<?php						
							$theme_path = get_template_directory_uri();
							$args = array( 'post_type' => 'resort','post_status' => 'publish', 'posts_per_page' => -1);    
							    $loop = new WP_Query( $args );
							    if($loop->have_posts()): 
							        $count = $loop->post_count;
							        while ( $loop->have_posts() ) : 
							            $loop->the_post();
							            $is_partner_hotel =  get_field('our_partner_hotel');  
							            $our_site_page = get_field('our_site_page');
							            $temp_resort_title = get_the_title(); 							          
							            $resort_details_link = get_the_permalink(); 
										if(empty($is_partner_hotel) && empty($our_site_page)){ ?>
											<li><a href="<?php echo $resort_details_link; ?>"><?php echo ucwords(strtolower($temp_resort_title)); ?></a></li>
										<?php
										}							            
							        endwhile; 
							    endif;
							    wp_reset_query();
							    echo '<li><a href="#">-------- OUR PARTNER HOTELS --------</a></li>';
						    	$loop = new WP_Query( $args );
							    if($loop->have_posts()): 
							        $count = $loop->post_count;
							        while ( $loop->have_posts() ) : 
							            $loop->the_post();
							            $is_partner_hotel =  get_field('our_partner_hotel');  
							            $temp_resort_title = get_the_title(); 							          
							            $resort_details_link = get_the_permalink(); 
										if(!empty($is_partner_hotel)){ ?>
											<li><a href="<?php echo $resort_details_link; ?>"><?php echo ucwords(strtolower($temp_resort_title)); ?></a></li>
										<?php
										}							            
							        endwhile; 
							    endif;
							    wp_reset_query();

						?>
    				</ul>
		</div>
  		<input type="text" name="search" placeholder="Search.."> 
		</form>
</div>