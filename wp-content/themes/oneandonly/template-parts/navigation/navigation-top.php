<?php
/**
 * Displays top navigation
 */

?>
<div class="collapse navbar-collapse" id="myNavbar">
<?php
if ( is_user_logged_in() ) {
   $defaults = array(
		'theme_location'  => 'login', 
		'items_wrap'      => '<ul>%3$s</ul>'
	);
} else {
	$defaults = array(
		'theme_location'  => 'header', 
		'items_wrap'      => '<ul>%3$s</ul>'
	);

}
echo wp_nav_menu($defaults ); 
?>
</div>

