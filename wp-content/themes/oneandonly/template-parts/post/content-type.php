<?php
//Today Date

date_default_timezone_set('Europe/London');
$sTime = date("d-m-Y");

global $post;
$theme_path = get_template_directory_uri();
global $catIdList;
$GLOBALS['catIdList'] = array();

$resort_type = '';
$CurrentUrl = get_permalink();
$image_display = $video_display = $marketing_display = $logo_display = "style='display:none';";
$resort_type = get_queried_object()->post_type;
// dd($resort_type);

if(isset($resort_type) && $resort_type != ''){
  if($resort_type == "resort_images"){
    $resort_type_value = "IMAGES";
    $image_display = "style='display:block';";
    $repiter_field = "add_image_data";
    $keyword_field = "image_keywords";
    $name_field    = "image_name";
    // $NewImageUrl = rtrim($CurrentUrl,"/").'?slug=image_name';
  }elseif($resort_type == "video"){
    $resort_type_value = "VIDEO";
    $video_display = "style='display:block';";
    $repiter_field = "add_video_data";
    $keyword_field = "video_keyword";
    $name_field    = "video_name";
    // $NewVideoUrl = rtrim($CurrentUrl,"/").'?type=video&slug=video_name';
  }elseif($resort_type == "marketing"){
    $resort_type_value = "MARKETING COLLATERAL";
    $marketing_display = "style='display:block';";
    $repiter_field = "add_marketing_collateral";
    $keyword_field = "marketing_collateral_keywords";
    $name_field    = "marketing_collateral_name";
    // $NewMarketingUrl = rtrim($CurrentUrl,"/").'?type=marketing&slug=marketing_name';
  }elseif($resort_type == "logo"){
    $resort_type_value = "LOGOS & MOTIFS";
    $logo_display = "style='display:block';";
    $repiter_field = "add_logo_data";
    $keyword_field = "logo_keywords";
    $name_field    = "logo_name";
    // $NewLogoUrl = rtrim($CurrentUrl,"/").'?type=logo&slug=logo_name';
  }
}


// $resort_slug = $post->post_name;
// $args = array(
//   'name'        => $resort_slug,
//   'post_type'   => 'resort',
//   'post_status' => 'publish',
//   'numberposts' => 1
// );
// $resort_posts = get_posts($args);
//
$resort_ID = get_the_ID();
$resort = explode(",", get_the_title());

$resort_title_location = $resort[0].', '.$resort[1];
$resort_title = $resort[0];

// $reosrt_slug = $resort_posts[0]->post_name;
$modelid = 0;

if( get_query_var('page') ) {
  $page = get_query_var( 'page' );
} else {
  $page = 1;
}

// $row              = 0;
// $resort_data_per_page  = 12; // How many images to display on each page
// $resort_data           = get_field( $repiter_field );
// $total            = count( $resort_data );
// $pages            = ceil( $total / $resort_data_per_page );
// $min              = ( ( $page * $resort_data_per_page ) - $resort_data_per_page ) + 1;
// $max              = ( $min + $resort_data_per_page ) - 1;

// dd(get_field('add_image_data'));
// dd($_REQUEST['mcat']);
if(isset($_REQUEST['mcat']) && $_REQUEST['mcat'] != '' ){
  $catInUrl = "mcat";
  getCatIdList($_REQUEST['mcat']);
  if($resort_type == "resort_images"){
    $resort_datas = shortRepiterDataByCategory($repiter_field,$resort_ID,'resort_image_main_category',$_REQUEST['mcat']);
  }elseif ($resort_type == "video") {
    $resort_datas = shortRepiterDataByCategory($repiter_field,$resort_ID,'resort_video_main_category',$_REQUEST['mcat']);
  }elseif ($resort_type == "marketing") {
    $resort_datas = shortRepiterDataByCategory($repiter_field,$resort_ID,'resort_marketing_collateral_main_category',$_REQUEST['mcat']);
  }elseif ($resort_type == "logo") {
    $resort_datas = shortRepiterDataByCategory($repiter_field,$resort_ID,'resort_logo_main_category',$_REQUEST['mcat']);
  }
}elseif (isset($_REQUEST['scat']) && $_REQUEST['scat'] != '' ) {
  $catInUrl = "scat";
  getCatIdList($_REQUEST['scat']);
  if($resort_type == "resort_images"){
    $resort_datas = shortRepiterDataByCategory($repiter_field,$resort_ID,'resort_image_sub_category',$_REQUEST['scat']);
  }
}elseif (isset($_REQUEST['sscat']) && $_REQUEST['sscat'] != '' ){
  $catInUrl = "sscat";
  getCatIdList($_REQUEST['sscat']);
  if($resort_type == "resort_images"){
    $resort_datas = shortRepiterDataByCategory($repiter_field,$resort_ID,'resort_image_super_sub_cat',$_REQUEST['sscat']);
  }
}else {
  $resort_datas = get_field($repiter_field);
  // remove Password data
  $resort_datas = removePasswordData($resort_datas);
}

$row              = 0;
$resort_data_per_page  = 12; // How many images to display on each page
// $resort_data           = get_field( $repiter_field );
$total            = count( $resort_datas );
$pages            = ceil( $total / $resort_data_per_page );
$min              = ( ( $page * $resort_data_per_page ) - $resort_data_per_page ) + 1;
$max              = ( $min + $resort_data_per_page ) - 1;
// dd($resort_datas);
// dd(get_term_by('id',get_term_by('id',get_term_by('id',178,'image_categories')->parent,'image_categories')->parent,'image_categories'));
// dd(get_term_by('id',get_term_by('id',178,'image_categories')->parent,'image_categories'));
// dd(get_term_by('id',178,'image_categories'));
// global $catIdList;
// $GLOBALS['catIdList'] = array();
function getCatIdList($id)
{
  $term = get_term_by('id',$id,'image_categories');
  array_push($GLOBALS['catIdList'], $term->term_id);
  if($term->parent != 0){
    getCatIdList($term->parent);
  }
}
global $wpdb;
$resortId = $wpdb->get_col("SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key` = 'select_resort_images' AND `meta_value` = ".get_the_ID(), 0, 'post_id');
$resortUrl = get_permalink($resortId[0]);
?>
<div class="content_container_filter col-lg-9 col-md-9 col-sm-9 col-xs-12">
  <div class="main_header_title">
    <h1><?php echo $resort_type_value; ?></h1>
    <h3><?php echo strtoupper($resort_title_location); ?></h3>
    <div class="divider"></div>
  </div>
  <div class="left_colmn col-lg-3 col-md-12 col-sm-12 col-xs-12">
    <div class="content_row_filters row">
      <div class="left_col_content_filter col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="left_col_detail_filter">
            <div class="left_col_detail_header">
              <h4><b>FILTER</b></h4>
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar2">
                <span class="icon-bar" style="background-color: #fff;"></span>
                <span class="icon-bar" style="background-color: #fff;""></span>
                <span class="icon-bar" style="background-color: #fff;""></span>
              </button>
            </div>
            <div class="collapse navbar-collapse edit-s" id="myNavbar2">
              <div class="left_col_acc">
                <button class="accordion <?php echo ($resort_type == 'resort_images')?'active':''; ?>">Images</button>
                <?php
                $resortCat = getImageNoneEmptyCat(get_the_ID());
                // $categorys = new FilterCategory(get_the_ID());
                // dd($categorys->getImageNoneEmptyCat());
                // dd(getImageNoneEmptyCat(get_the_ID()));
                 ?>
                <div class="main_panel" <?php echo ($resort_type == 'resort_images')?'style="display: block;"':''; ?>>
                  <?php
                  foreach( get_terms( 'image_categories', array( 'hide_empty' => false, 'parent' => 0, 'order' => 'ASC' ) ) as $parent_term ) {

                    $term_children = get_term_children($parent_term->term_id, 'image_categories');

                    if(in_array($parent_term->term_id, $resortCat)){
                    if($term_children){
                      $mcatactive = (in_array($parent_term->term_id,$GLOBALS['catIdList']))?'active':'';
                      $mactdisplay = (in_array($parent_term->term_id,$GLOBALS['catIdList']))?'style="display: block;"':'';
                      echo '<button class="accordion_2 '.$mcatactive.'" ><a href="'.rtrim($CurrentUrl,"/").'?mcat='.$parent_term->term_id.'" style="color: #FFF" onClick="addPassword('.$parent_term->term_id.','. $resort_ID .',event)">'.$parent_term->name.'</a></button>';
                      echo ' <div class="panel noborder" '.$mactdisplay.'>';
                      foreach( get_terms( 'image_categories', array( 'hide_empty' => false, 'parent' => $parent_term->term_id ) ) as $child_term ) {
                        $term_super_children = get_term_children($child_term->term_id, 'image_categories');
                        if(in_array($child_term->term_id, $resortCat)){
                        if($term_super_children){
                          $scatactive = (in_array($child_term->term_id,$GLOBALS['catIdList']))?'active':'';
                          $scatdisplay = (in_array($child_term->term_id,$GLOBALS['catIdList']))?'style="display: block;"':'';
                          echo '<button class="accordion_2 '.$scatactive.'"><a href="'.rtrim($CurrentUrl,"/").'?scat='.$child_term->term_id.'" style="color: #FFF">'.$child_term->name.'</a></button>';
                          echo ' <div class="panel noborder" '.$scatdisplay.'>';
                          foreach (get_terms( 'image_categories', array( 'hide_empty' => false, 'parent' => $child_term->term_id ) ) as $superChild_term) {
                            if(in_array($superChild_term->term_id, $resortCat)){
                            echo '<a href="'.rtrim($CurrentUrl,"/").'?sscat='.$superChild_term->term_id.'" class="left_col_button">'.$superChild_term->name.'</a>';
                            }
                          }
                          echo '</div>';
                        }else{
                          echo '<a href="'.rtrim($CurrentUrl,"/").'?scat='.$child_term->term_id.'" class="left_col_button">'.$child_term->name.'</a>';
                        }
                       }
                      }
                      echo '</div>';
                    }else{
                      echo '<a href="'.rtrim($CurrentUrl,"/").'?mcat='.$parent_term->term_id.'" class="left_col_button" onClick="addPassword('.$parent_term->term_id.','. $resort_ID .',event)">'.$parent_term->name.'</a>';
                    }
                    }
                  }
                  ?>
                </div>

                <button class="accordion">Videos</button>
                <div class="main_panel">
                  <?php
                  $resortVideoCat = getVideoNoneEmptyCat($resortId[0]);
                  foreach( get_terms( 'video_categories', array( 'hide_empty' => false, 'parent' => 0, 'order' => 'ASC' ) ) as $parent_term ) {
                    if (in_array($parent_term->term_id, $resortVideoCat)) {
                      echo '<a href="'.rtrim($resortUrl,"/").'?type=video&mcat='.$parent_term->term_id.'" class="left_col_button">'.$parent_term->name.'</a>';
                    }
                  }
                  ?>
                </div>

                <button class="accordion">Marketing Collateral</button>
                <div class="main_panel">
                  <?php
                  $resortMarkatingCat = getMarketingNoneEmptyCat($resortId[0]);
                  foreach( get_terms( 'marketing_categories', array( 'hide_empty' => false, 'parent' => 0, 'order' => 'ASC' ) ) as $parent_term ) {
                    if (in_array($parent_term->term_id, $resortMarkatingCat)) {
                    echo '<a href="'.rtrim($resortUrl,"/").'?type=marketing&mcat='.$parent_term->term_id.'" class="left_col_button">'.$parent_term->name.'</a>';
                    }
                  }
                  ?>
                </div>

                <button class="accordion noborder">Logos & Motifs</button>
                <div class="main_panel">
                  <?php
                  $resortLogoCat = getLogoNoneEmptyCat($resortId[0]);
                  foreach( get_terms( 'logo_categories', array( 'hide_empty' => false, 'parent' => 0, 'order' => 'ASC' ) ) as $parent_term ) {
                    if (in_array($parent_term->term_id, $resortLogoCat)) {
                    echo '<a href="'.rtrim($resortUrl,"/").'?type=logo&mcat='.$parent_term->term_id.'" class="left_col_button">'.$parent_term->name.'</a>';
                    }
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content_container_right container col-lg-9 col-md-12 col-sm-12 col-xs-12">
    <div class="row col_row_filter">
      <div class="content_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="image_listing_only" <?php echo $image_display; ?>>
            <?php if(!empty($resort_datas)): foreach($resort_datas as $resort_data): ?>
              <?php $row++; ?>
              <?php if($row < $min) { continue; } ?>
              <?php if($row > $max) { break; } ?>
            <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
              <div class="">
                <!-- <a href="<?php echo rtrim($CurrentUrl,"/").'?slug='.$resort_data[$keyword_field]; ?>"><img src="<?php echo $resort_data['add_low_resolution_web']['url']; ?>" alt="Avatar" class="content_images_search_img"></a> -->
                <a href="<?php echo rtrim($CurrentUrl,"/").'?slug='.$resort_data[$keyword_field]; ?>"><img src="<?php echo $resort_data['add_low_resolution_web']['sizes']['resort_image_thumbnail']; ?>" alt="Avatar" class="content_images_search_img"></a>
                <div class="image_detail_search">
                  <a href="<?php echo rtrim($CurrentUrl,"/").'?slug='.$resort_data[$keyword_field]; ?>">
                    <h4 class="content_search_desc"><b><?php echo $resort_title; ?></b></h4>
                  </a>
                  <h5 class="content_search_desc"><?php echo $resort_data[$name_field] ?></h5>
                  <div class="ref_detail">
                    <div class="ref_detail_text">
                      <span>Added <?php echo getAcfFileDate($resort_data['add_low_resolution_web']); ?></span><br>
                    </div>
                    <div class="detail_icons img_search">
                      <!-- Button trigger modal -->
                      <button type="button" class="cart-detail" data-toggle="modal" data-target="#i<?php echo $modelid ?>" style="float:  left;">
                        <img src="<?php echo $theme_path.'/assets/img/cart_icon.png'; ?>" class="image_detail_icons">
                        <span><?php echo resortPageCount( $resort_data[$keyword_field], get_the_ID(), $resort_type ); ?></span>
                      </button>
                      <!-- Modal -->
                      <div class="modal fade cust" id="i<?php echo $modelid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-body">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <img src="<?php echo $theme_path.'/assets/img/close-pop.png'; ?>">
                              </button>
                              <img src="<?php echo $resort_data['add_low_resolution_web']['url']; ?>">
                              <div class="contantIn">
                                <h2><b><?php echo $resort_title; ?></b></h2>
                                <h4><?php echo $resort_data[$name_field] ?></h4>
                                <div class="versionAvilable">
                                  <h3>Versions available:</h3>
                                  <?php if($resort_data['add_low_resolution_web']) : ?>
                                    <div class="versionAvilableLine">
                                      <h3>Low Resolution (Web)</h3>
                                      <div class="inputRadio">
                                        <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_low_resolution_web']['url'] ?>" name="selectImageRes">
                                      </div>
                                    </div>
                                    <?php
                                  endif;
                                  if($resort_data['add_mid_resolution_print']) :
                                    ?>
                                    <div class="versionAvilableLine">
                                      <h3>Mid Resolution (Web)</h3>
                                      <div class="inputRadio">
                                        <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_mid_resolution_print']['url'] ?>" name="selectImageRes">
                                      </div>
                                    </div>
                                    <?php
                                  endif;
                                  if($resort_data['add_high_resolution_print']) :
                                    ?>
                                    <div class="versionAvilableLine">
                                      <h3>High Resolution (Print)</h3>
                                      <div class="inputRadio">
                                        <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_high_resolution_print']['url'] ?>" name="selectImageRes">
                                      </div>
                                    </div>
                                    <?php
                                  endif;
                                  if($resort_data['add_max_resolution_print']) :
                                    ?>
                                    <div class="versionAvilableLine">
                                      <h3>Max Resolution (Print)</h3>
                                      <div class="inputRadio">
                                        <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_max_resolution_print']['url'] ?>" name="selectImageRes">
                                      </div>
                                    </div>
                                  <?php endif; ?>
                                  <div class="footerBtn">
                                    <a class="add-to-cart" role="button" data-type="<?php echo $resort_type ?>" data-postid="<?php echo get_the_ID() ?>" data-keyword="<?php echo $resort_data[$keyword_field] ?>" data-dismiss="modal" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample">SELECT</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="image-download">
                        <?php
                        global $wp;
                        $current_url = home_url(add_query_arg(array(),$wp->request)); ?>
                        <input type="hidden" name="image_url" value="<?php echo $current_url ?>">
                        <input type="hidden" name="action" value="single_resort_image_download_assets">
                        <input type="hidden" name="postid" value="<?php echo get_the_ID() ?>">
                        <input type="hidden" name="keyword" value="<?php echo $resort_data[$keyword_field] ?>">
                        <input type="hidden" name="resort_name" value="<?php echo $resort_data[$name_field] ?>">
                        <input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
                      <!-- <a href="javascript:void(0)"><img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons"></a> -->
                      <?php if($resort_data['add_low_resolution_web']) : ?>
                        <input type="hidden" name="images[]" value="<?php echo $resort_data['add_low_resolution_web']['url'] ?>">
                        <?php endif; if($resort_data['add_mid_resolution_print']) : ?>
                          <input type="hidden" name="images[]" value="<?php echo $resort_data['add_mid_resolution_print']['url'] ?>">
                        <?php endif; if($resort_data['add_high_resolution_print']) : ?>
                          <input type="hidden" name="images[]" value="<?php echo $resort_data['add_high_resolution_print']['url'] ?>">
                        <?php endif; if($resort_data['add_max_resolution_print']) : ?>
                          <input type="hidden" name="images[]" value="<?php echo $resort_data['add_max_resolution_print']['url'] ?>">
                        <?php endif; ?>
                        <?php if(is_user_logged_in()) : ?>
                        <button type="submit" class="download-btn cart-detail">
                          <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                        </button>
                        <?php else: ?>
                          <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                            <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          </a>
                        <?php endif; ?>
                      </form>
                      <?php $noImageSelect =  $_SESSION['noImageSelect'];
                      	if($noImageSelect == $resort_data[$keyword_field]) { ?>
                        	<div class="all_bsk_err_msg">
                          		<span>Please select an image resolution</span>
                        	</div>
                        	<?php  unset($_SESSION['noImageSelect']);
                      	}
                      	if($_SESSION['imageAdd'] == $resort_data[$keyword_field]){ ?>
                        	<div class="all_bsk_succ_msg session_close">
                          		<span>Your Product added to cart</span>
                          		<button class="bsk_msg_close"><i class="fa fa-times"></i></button>
                        	</div>
                      	<?php unset($_SESSION['imageAdd']); } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php $modelid++; endforeach; endif; ?>
        </div>

        <div id="video_listing_only" <?php echo $video_display; ?>>
          <?php if(!empty($resort_datas)): foreach($resort_datas as $resort_data): ?>
            <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
              <div class="">
                <video width="100%" height="auto" controls controlsList="nodownload">
                  <source src="<?php echo $resort_data['add_low_resolution_video_360p']['url'] ?>" type="video/mp4">
                    Your browser does not support HTML5 video.
                  </video>
                  <?php  //echo"<pre>";  var_dump(explode(' ',$resort_data['add_low_resolution_video_360p']['date'])[0]); die(); ?>

                    <div class="image_detail_search">
                      <a href="<?php echo rtrim($CurrentUrl,"/").'?type=video&slug='.$resort_data[$keyword_field]; ?>">
                        <h4><b><?php echo $resort_title; ?></b></h4>
                      </a>
                      <h5 class="content_search_desc"><?php echo $resort_data[$name_field] ?></h5>
                      <div class="ref_detail">
                        <div class="ref_detail_text">
                          <span>Added <?php echo getAcfFileDate($resort_data['add_low_resolution_video_360p']); ?></span><br>
                        </div>
                        <div class="detail_icons img_search">
                          <!-- Button trigger modal -->
                          <button type="button" style="float: left" class="cart-detail" data-toggle="modal" data-target="#v<?php echo $modelid ?>">
                            <img src="<?php echo $theme_path.'/assets/img/cart_icon.png'; ?>" class="image_detail_icons">
                            <span><?php echo resortPageCount( $resort_data[$keyword_field], get_the_ID(), $resort_type ); ?></span>
                          </button>
                          <div class="modal fade cust" id="v<?php echo $modelid ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                                    <img src="<?php echo $theme_path.'/assets/img/close-pop.png'; ?>">
                                  </button>
                                  <!-- <img src="<?php echo $theme_path.'/assets/img/home_exterior2.jpg'; ?>"> -->
                                  <video width="100%" height="auto" controls controlsList="nodownload">
                                    <source src="<?php echo $resort_data['add_low_resolution_video_360p']['url'] ?>" type="video/mp4">
                                      Your browser does not support HTML5 video.
                                    </video>
                                  <div class="contantIn">
                                    <h2><?php echo $resort_title; ?> </h2>
                                    <h4><?php echo $resort_data[$name_field] ?></h4>
                                    <div class="versionAvilable">
                                      <h3>Versions available:</h3>
                                      <?php if ($resort_data['add_low_resolution_video_360p']): ?>
                                        <div class="versionAvilableLine">
                                          <h3>Low Resolution 360P </h3>
                                          <div class="inputRadio">
                                            <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_low_resolution_video_360p']['url'] ?>" name="selectVideoRes">&nbsp;</label>
                                          </div>
                                        </div>
                                      <?php endif; ?>
                                      <?php if ($resort_data['add_mid_resolution_video_720p']): ?>
                                      <div class="versionAvilableLine">
                                        <h3>Mid Resolution 720P </h3>
                                        <div class="inputRadio">
                                          <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_mid_resolution_video_720p']['url'] ?>" name="selectVideoRes">&nbsp;</label>
                                        </div>
                                      </div>
                                      <?php endif; ?>
                                      <?php if ($resort_data['add_high_resolution_video_1080p']): ?>
                                      <div class="versionAvilableLine">
                                        <h3>High Resolution 1080P </h3>
                                        <div class="inputRadio">
                                          <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_high_resolution_video_1080p']['url'] ?>" name="selectVideoRes">&nbsp;</label>
                                        </div>
                                      </div>
                                    <?php endif; ?>
                                      <div class="footerBtn">
                                        <a class="add-to-cart" role="button" data-type="<?php echo $resort_type ?>" data-postid="<?php echo get_the_ID() ?>" data-keyword="<?php echo $resort_data[$keyword_field] ?>" data-dismiss="modal" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample">SELECT</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="video-download">
                            <input type="hidden" name="action" value="single_resort_download_assets">
                            <input type="hidden" name="postid" value="<?php echo get_the_ID() ?>">
                            <input type="hidden" name="keyword" value="<?php echo $resort_data[$keyword_field] ?>">
                            <input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
                            <?php if ($resort_data['add_low_resolution_video_360p']): ?>
                              <input type="hidden" name="videos[]" value="<?php echo $resort_data['add_low_resolution_video_360p']['url'] ?>">
                            <?php endif;if ($resort_data['add_mid_resolution_video_720p']): ?>
                              <input type="hidden" name="videos[]" value="<?php echo $resort_data['add_mid_resolution_video_720p']['url'] ?>">
                            <?php endif;if ($resort_data['add_high_resolution_video_1080p']): ?>
                                <input type="hidden" name="videos[]" value="<?php echo $resort_data['add_high_resolution_video_1080p']['url'] ?>">
                              <?php endif; ?>
                          <!-- <a href="javascript:void(0)"><img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons"></a> -->
                          <?php if(is_user_logged_in()) : ?>
                          <button type="submit" class="download-btn">
                            <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          </button>
                        <?php else: ?>
                          <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                            <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                          </a>
                        <?php endif; ?>
                        </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php $modelid++; endforeach; endif; ?>
            </div>

            <div id="marketing_listing_only" <?php echo $marketing_display; ?>>
              <?php if(!empty($resort_datas)): foreach($resort_datas as $resort_data): ?>
                <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
                  <div class="">
                    <a href="<?php echo rtrim($CurrentUrl,"/").'?type=marketing&slug='.$resort_data[$keyword_field]; ?>"><img src="<?php echo $resort_data['add_marketing_collateral_file']['url']; ?>" alt="Avatar" class="image" style="width:100%"></a>
                    <div class="image_detail_search">
                      <h4><b><?php echo $resort_title; ?></b></h4>
                      <h5 class="content_search_desc"><?php echo $resort_data[$name_field] ?></h5>
                      <div class="ref_detail">
                        <div class="ref_detail_text">
                          <span>Added <?php echo getAcfFileDate($resort_data['add_marketing_collateral_file']); ?></span><br>
                          <span>5000 x 3000 TIFF</span>
                        </div>
                        <div class="detail_icons img_search">
                          <!-- Button trigger modal -->
                          <button type="button" style="float: left" class="cart-detail" data-toggle="modal" data-target="#m<?php echo $modelid ?>">
                            <img src="<?php echo $theme_path.'/assets/img/cart_icon.png'; ?>" class="image_detail_icons">
                            <span><?php echo resortPageCount( $resort_data[$keyword_field], get_the_ID(), $resort_type ); ?></span>
                          </button>
                          <!-- Modal -->
                          <div class="modal fade cust" id="m<?php echo $modelid ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <img src="<?php echo $theme_path.'/assets/img/close-pop.png'; ?>">
                                  </button>
                                  <img src="<?php echo $resort_data['add_marketing_collateral_file']['url']; ?>">
                                  <div class="contantIn">
                                    <h2><?php echo $resort_title; ?> </h2>
                                    <h4><?php echo $resort_data[$name_field] ?></h4>
                                    <div class="versionAvilable">
                                      <h3>Versions available:</h3>
                                      <?php if ($resort_data['add_marketing_collateral_file']): ?>
                                        <div class="versionAvilableLine">
                                          <h3>File (PDF) 1.3MB</h3>
                                          <div class="inputRadio">
                                            <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_marketing_collateral_file']['url'] ?>" name="selectMarketingRes">&nbsp;</label>
                                          </div>
                                        </div>
                                      <?php endif; ?>
                                      <div class="footerBtn">
                                        <a class="add-to-cart" role="button" data-type="<?php echo $resort_type ?>" data-postid="<?php echo get_the_ID() ?>" data-keyword="<?php echo $resort_data[$keyword_field] ?>" data-dismiss="modal" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample">SELECT</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="marketing-download">
                            <input type="hidden" name="action" value="single_resort_download_assets">
                            <input type="hidden" name="postid" value="<?php echo get_the_ID() ?>">
                            <input type="hidden" name="keyword" value="<?php echo $resort_data[$keyword_field] ?>">
                            <input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
                            <?php if ($resort_data['add_marketing_collateral_file']): ?>
                              <input type="hidden" name="marketing[]" value="<?php echo $resort_data['add_marketing_collateral_file']['url'] ?>">
                            <?php endif; ?>
                            <?php if(is_user_logged_in()) : ?>
                            <button type="submit" class="download-btn">
                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                            </button>
                          <?php else: ?>
                            <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                            </a>
                          <?php endif; ?>
                          <!-- <a href="javascript:void(0)"><img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons"></a> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php $modelid++; endforeach;endif; ?>
            </div>

            <div id="logo_listing_only" <?php echo $logo_display; ?>>
              <?php if(!empty($resort_datas)): foreach($resort_datas as $resort_data): ?>
                <div class="content_images_search col-lg-9 col-md-9 col-sm-9 col-xs-12">
                  <div class="">
                    <a href="<?php echo rtrim($CurrentUrl,"/").'?type=logo&slug='.$resort_data['logo_keywords']; ?>"><img src="<?php echo $resort_data['add_low_resolution_logo_print_72kb']['url']; ?>" alt="Avatar" class="image" style="width:100%"></a>
                    <div class="image_detail_search">
                      <h4><b><?php echo $resort_title; ?></b></h4>
                      <h5 class="content_search_desc"><?php echo $resort_data[$name_field] ?></h5>
                      <div class="ref_detail">
                        <div class="ref_detail_text">
                          <span>Added <?php  echo getAcfFileDate($resort_data['add_low_resolution_logo_print_72kb']); ?></span><br>
                          <span>5000 x 3000 TIFF</span>
                        </div>
                        <div class="detail_icons img_search">
                          <!-- Button trigger modal -->
                          <button type="button" style="float: left" class="cart-detail" data-toggle="modal" data-target="#l<?php echo $modelid ?>">
                            <img src="<?php echo $theme_path.'/assets/img/cart_icon.png'; ?>" class="image_detail_icons">
                            <span><?php echo resortPageCount( $resort_data[$keyword_field], get_the_ID(), $resort_type ); ?></span>
                          </button>
                          <!-- Modal -->
                          <div class="modal fade cust" id="l<?php echo $modelid ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-body">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <img src="<?php echo $theme_path.'/assets/img/close-pop.png'; ?>">
                                  </button>
                                  <img src="<?php echo $resort_data['add_low_resolution_logo_print_72kb']['url']; ?>">
                                  <div class="contantIn">
                                    <h2><?php echo $resort_title; ?> </h2>
                                    <h4><?php echo $resort_data[$name_field] ?></h4>
                                    <div class="versionAvilable">
                                      <h3>Versions available:</h3>
                                      <?php if ($resort_data['add_low_resolution_logo_print_72kb']): ?>
                                        <div class="versionAvilableLine">
                                          <h3>Low Resolution (web) 78KB</h3>
                                          <div class="inputRadio">
                                            <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_low_resolution_logo_print_72kb']['url'] ?>" name="selectImageRes">&nbsp;</label>
                                          </div>
                                        </div>
                                      <?php endif; ?>
                                      <?php if ($resort_data['add_high_resolution_logo_print_370kb']): ?>
                                        <div class="versionAvilableLine">
                                          <h3>Mid Resolution (print) 370KB</h3>
                                          <div class="inputRadio">
                                            <input type="checkbox"style="opacity:1" class="chk" value="<?php echo $resort_data['add_high_resolution_logo_print_370kb']['url'] ?>" name="selectImageRes">&nbsp;</label>
                                          </div>
                                        </div>
                                      <?php endif; ?>
                                      <div class="footerBtn">
                                        <a class="add-to-cart" role="button" data-type="<?php echo $resort_type ?>" data-postid="<?php echo get_the_ID() ?>" data-keyword="<?php echo $resort_data[$keyword_field] ?>" data-dismiss="modal" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample">SELECT</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <form action="<?php echo get_site_url().'/wp-admin/admin-post.php'; ?>" id="logo-download">
                            <input type="hidden" name="action" value="single_resort_download_assets">
                            <input type="hidden" name="postid" value="<?php echo get_the_ID() ?>">
                            <input type="hidden" name="keyword" value="<?php echo $resort_data[$keyword_field] ?>">
                            <input type="hidden" name="type" value="<?php echo $_REQUEST['type'] ?>">
                            <?php if($resort_data['add_low_resolution_logo_print_72kb']) : ?>
                              <input type="hidden" name="logos[]" value="<?php echo $resort_data['add_low_resolution_logo_print_72kb']['url'] ?>">
                            <?php endif; if ($resort_data['add_high_resolution_logo_print_370kb']): ?>
                              <input type="hidden" name="logos[]" value="<?php echo $resort_data['add_high_resolution_logo_print_370kb']['url'] ?>">
                            <?php endif; ?>
                            <?php if(is_user_logged_in()) : ?>
                            <button type="submit" class="download-btn">
                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                            </button>
                          <?php else: ?>
                            <a class="" role="button" data-toggle="collapse" href="javascript:void(0)" aria-expanded="false" aria-controls="collapseExample1" onclick="alert('For Download, Please Login First !')">
                              <img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons">
                            </a>
                          <?php endif; ?>
                          </form>
                          <!-- <a href="javascript:void(0)"><img src="<?php echo $theme_path.'/assets/img/download_icon.png'; ?>" class="image_detail_icons"></a> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php $modelid++; endforeach; endif; ?>
            </div>
          </div>
          <div class="cust_pagination">
            <ul>
              <?php $urlCat = (isset($catInUrl))? "&".$catInUrl."=".$_REQUEST[$catInUrl] : ""; ?>
              <?php if($pages > 1): ?>
              <?php if($page > 1): ?>
                <li><a href="<?php echo get_permalink().($page-1).'/?type='.$resort_type.$urlCat  ?>" class=""><img src="<?php echo $theme_path.'/assets/img/left.png'; ?>"></a></li>
              <?php endif; ?>
              <li><?php echo $page ?></li>
              <li><span>of</span></li>
              <li><a href="<?php echo get_permalink().$pages.'/?type='.$resort_type.$urlCat ?>" class=""><?php echo $pages ?></a></li>
              <?php if($page < $pages): ?>
              <li><a href="<?php echo get_permalink().($page+1).'/?type='.$resort_type.$urlCat  ?>" class=""><img src="<?php echo $theme_path.'/assets/img/right.png'; ?>"></a></li>
            <?php endif; ?>
          <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <script>
    // $(document).ready(function () {
    //   modal
    // });
    $('.modal').on('hidden.bs.modal', function (e) {
      window.location.reload();
    })
    var acc = document.getElementsByClassName("accordion");

    var i;

    for (i = 0; i < acc.length; i++) {

      acc[i].onclick = function(){

        this.classList.toggle("active");

        var panel = this.nextElementSibling;

        if (panel.style.display === "block") {

          panel.style.display = "none";

        } else {

          panel.style.display = "block";

        }

      }

    }

    var acc = document.getElementsByClassName("accordion_2");

    var i;

    for (i = 0; i < acc.length; i++) {

      acc[i].onclick = function(){

        this.classList.toggle("active");

        var panel = this.nextElementSibling;

        if (panel.style.display === "block") {

          panel.style.display = "none";

        } else {

          panel.style.display = "block";

        }

      }

    }

    function myFunction() {

      var popup = document.getElementById("myPopup");

      popup.classList.toggle("show");

    }

    $('button.bsk_msg_close').on('click', function () {
        var elements = document.getElementsByClassName('all_bsk_succ_msg');
        for(var i=0; i<elements.length; i++) {
          elements[i].style.display='none';
        }
        //return false;
    });

    </script>
